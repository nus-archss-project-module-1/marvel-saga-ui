NPM & Node JS -> https://nodejs.org/en/download/
Mysql - > https://dev.mysql.com/doc/mysql-installation-excerpt/5.7/en/mysql-installer-setup.html
maven -> https://maven.apache.org/guides/getting-started/windows-prerequisites.html
Java -> https://java.com/en/download/help/download_options.html
Editor -> https://www.jetbrains.com/idea/download/#section=windows


### Local start up commands
npm install -g webpack
npm install -g webpack-dev-server
npm install

### Usage

### Available Commands
```
npm start - start the production server
npm run dev - start local development server
npm run build - create a production ready build in `dist` folder
```

### Docker Build - to build using the docker path
docker build -t registry-1.docker.io/veenu143/marvel-saga-ui:latest .

### Docker Run - only to run the image
docker run -d -it  -p 8080:8080/tcp --name react-app registry-1.docker.io/veenu143/marvel-saga-ui:latest

###Docker See process
docker ps
docker container ls -a

###Docker Stop
docker stop react-app
docker container stop react-app

 
### Docker Logs
docker container logs react-app


### Docker Build
 docker build -t marvel-saga-ui-latest .
 docker tag marvel-saga-ui-latest veenu143/marvel-saga-ui:latest 
 docker push veenu143/marvel-saga-ui:latest 

## Start Kubernetes deployment
kubectl apply -f kube/saga-ui/

## Delete Kubernetes Deployment
kubectl delete -f kube/saga-ui/

 
## Certificate
https://stackoverflow.com/questions/26663404/webpack-dev-server-running-on-https-web-sockets-secure


