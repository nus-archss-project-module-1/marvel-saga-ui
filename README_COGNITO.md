step 1: npm install -g @aws-amplify/cli

step 2: amplify configure
- login aws console
- choose region
- choose username: serverless-amplify (marvel-security-amp)
- click next
- click next
- click next
- click review
- click add user
- copy access key id
- copy secret access key
- put profile name: serverless-amplify (marvel-security-amp)

step 3: amplify init
- add project name: amplifyreactapp (marvelsagaui)
- initialize: no
- environment: dev
- default editor: intellij idea
- type app: react
- directory: src
- distribution: build
- build command: npm run-script build
- start command: npm run-script start
- aws profile: Y
- choose profile: serverless-amplify

step 4: amplify add auth
- default configuration
- sign in: email
- advanced setting: no, i am done

step 5: amplify push --y
- continue: y
- check the aws-exports.js will have cognito user pool

step 6: npm install --save aws-amplify @aws-amplify/ui-react

