# pull official base image
FROM node:14.18.0-alpine

# set working directory
WORKDIR /usr/src/app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /usr/src/app/node_modules/.bin:$PATH

# install app dependencies
COPY package*.json ./
ADD package.json /usr/src/app/package.json
RUN npm install -g webpack
RUN npm install -g webpack-dev-server
RUN npm install --no-optional && npm cache clean --force

# add app
COPY . .

EXPOSE 443
EXPOSE 8080
# start app
CMD ["npm", "start"]