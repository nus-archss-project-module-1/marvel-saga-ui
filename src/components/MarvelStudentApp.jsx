import React from 'react';
import Loading from './common/Loading'
import StudentsHeaderBar from "./students/StudentsHeaderBar";
import Navigation from "../core/Navigation";


let MarvelStudentApp = () => (
    <div>
        <StudentsHeaderBar/>
        <Loading/>
        <Navigation/>
    </div>
);


export default MarvelStudentApp;
