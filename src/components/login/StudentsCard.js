import React from 'react';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import './login.css';
import TextField from '@material-ui/core/TextField';
import {withRouter} from "react-router-dom";
import {STUDENT_LOGIN} from "../../actions/ActionConstants";
import {connect} from "react-redux";

import {Auth} from 'aws-amplify';
import {Avatar} from "@material-ui/core";

class StudentsCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loginDetails: {
                userName: '',
                userPassword: '',
                loginType: '',
                email: '',
                authCode: '',
                formType: 'signUp',
                moduleType: 'student',
                token: '',
            },
            errorMessage: '',
            isSubmitted: false,
            isUserLoginExist: false,
        }
    }

    async componentDidMount() {
        console.log("Check existing user session")

        const studentUser = await Auth.currentAuthenticatedUser()
        const studentSession = await Auth.currentSession();

        const userInfo = await Auth.currentUserInfo()
        const email = userInfo.attributes.email;

        if (this.state.loginDetails.moduleType === 'student' && email === 'nusmtechse21@gmail.com' &&
            studentUser !== '' && studentSession !== null) {
            this.props.history.push('/studentDashboard');
        }
    }

    handleOnChange() {
        return (e) => {
            e.preventDefault();
            const {name, value} = e.target;
            const loginDetails = this.state.loginDetails;
            loginDetails[name] = value;
            this.setState({loginDetails});
        };
    }

    async checkUserInDB() {
        //if(this.state.isUserLoginExist) {
        // Check user against database
        try {
            console.log('prepare check user against db');

            const userInfo = await Auth.currentUserInfo()
            const email = userInfo.attributes.email;
            const username = userInfo.username;
            const loginType = username.substring(0, username.indexOf("_"));

            const studentSession = await Auth.currentSession();
            this.state.loginDetails.token = studentSession.getAccessToken().getJwtToken();

            this.state.loginDetails.userName = email;
            this.state.loginDetails.loginType = loginType;

            console.log('this.state.loginDetails.userName', email);
            console.log('this.state.loginDetails.loginType', loginType);

            await this.props.studentLogin(this.state);
            console.log('after checking user in db');
        } catch (error) {
            this.setState({errorMessage: 'Invalid Username/Password', isSubmitted: false})
            console.log('error signing in', error);
        }
        // } else {
        //     console.log('not calling check user against db');
        // }
    }

    render() {
        const goRegister = (event) => {
            event.preventDefault();
            this.props.history.push('/registerStudent');
        }

        const onLogin = async (event) => {
            event.preventDefault();
            try {

                if (this.state.loginDetails.userName === '' || this.state.loginDetails.userPassword === '') {
                    this.setState({errorMessage: 'UserName/Password cannot be empty', isSubmitted: false})
                    return;
                }

                if (this.state.loginDetails.userName !== '') {
                    const pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
                    if (!pattern.test(this.state.loginDetails.userName)) {
                        this.setState({errorMessage: 'UserName must a valid email address', isSubmitted: false})
                        return;
                    }
                }

                if(this.state.loginDetails.userPassword !== ''){
                    const isValid = /^[0-9a-zA-Z]+$/i.test(this.state.loginDetails.userPassword);
                    if (!isValid) {
                        this.setState({errorMessage: 'Username/Password has invalid characters', isSubmitted: false})
                        return;
                    }
                }


                console.log('prepare signing in Cognito');
                console.log('this.state.loginDetails.userName', this.state.loginDetails.userName);
                console.log('this.state.loginDetails.userPassword', this.state.loginDetails.userPassword);
                const user = await Auth.signIn(this.state.loginDetails.userName, this.state.loginDetails.userPassword);
                this.state.loginDetails.loginType = "custom";

                const session = await Auth.currentSession();
                this.state.loginDetails.token = session.getAccessToken().getJwtToken();

                this.setState({isUserLoginExist: true})
                console.log('successfully signing in Cognito');
            } catch (error) {
                this.setState({errorMessage: 'User doesnt exist', isUserLoginExist: false})
                console.log('no user in Cognito', error);
            }
            // }

            if (this.state.isUserLoginExist) {
                try {
                    console.log('prepare check user against db');
                    await this.props.studentLogin(this.state);
                    console.log('after checking user in db');
                } catch (error) {
                    this.setState({errorMessage: 'Invalid Username/Password', isSubmitted: false})
                    console.log('error signing in', error);
                }
            }

            console.log('user exist in cognito and in db');
            console.log('aaa this.props.isStudentLoginSuccessful', this.props.isStudentLoginSuccessful);
            this.setState({isSubmitted: true})
        }

        const onLoginFacebook = async (event) => {
            event.preventDefault();
            // this.setState({errorMessage: ''})
            // this.setState({isUserLoginExist: false})

            // if(this.state.errorMessage === '') {
            // Login to Facebook
            try {
                console.log('prepare signing in Facebook');
                const user = await Auth.federatedSignIn({provider: 'Facebook'});
                console.log('successfully signing in Facebook');

                const session = await Auth.currentSession();
                this.state.loginDetails.token = session.getAccessToken().getJwtToken();

                // this.state.loginDetails.loginType = "facebook";
                this.setState({isUserLoginExist: true})
            } catch (error) {
                this.setState({errorMessage: 'User doesnt exist in Facebook', isUserLoginExist: false})
                console.log('no user in Facebook', error);
            }
            // }
            this.setState({isSubmitted: true})
        }

        const onLoginGoogle = async (event) => {
            event.preventDefault();
            // this.setState({errorMessage: ''})

            try {
                // if(this.state.errorMessage === '') {
                // Login to Cognito
                console.log('prepare signing in Google');
                const user = await Auth.federatedSignIn({provider: 'Google'});
                console.log('successfully signing in Google');

                const session = await Auth.currentSession();
                this.state.loginDetails.token = session.getAccessToken().getJwtToken();

                // this.state.loginDetails.loginType = "google";
                this.setState({isUserLoginExist: true})
                // }
            } catch (error) {
                this.setState({errorMessage: 'User doesnt exist in Google', isUserLoginExist: false})
                console.log('no user in Google', error);
            }
            //await this.props.studentLogin(this.state);
            this.setState({isSubmitted: true})
        }


        if (this.state.isSubmitted && this.props.isStudentLoginSuccessful === 'No current user') {
            // console.log('isSubmitted from isStudentLoginSuccessful ', this.props.isStudentLoginSuccessful);
            this.setState({errorMessage: 'No current user', isSubmitted: false})
        }

        if (this.state.loginDetails.moduleType === 'student' && this.state.isSubmitted && this.props.isStudentLoginSuccessful === 'Success') {
            this.props.history.push('/studentDashboard');
        }

        if (this.state.isSubmitted && this.props.isStudentLoginSuccessful === "Invalid username/password") {
            // this.setState({isSubmitted: false})
            console.log('come to isStudentLoginSuccessful', this.props.isStudentLoginSuccessful);
            console.log('isSubmitted', this.state.isSubmitted);
            console.log('loginType', this.state.loginDetails.loginType);

            if (this.state.loginDetails.loginType === "custom") {
                this.setState({errorMessage: 'Invalid Username/Password', isSubmitted: false})
            } else {
                this.props.history.push('/registerStudent');
            }
        }

        return (
            <Card className="loginClass">
                <CardActionArea>
                    <CardMedia
                        component="img"
                        alt="Marvel Student"
                        height="150"
                        image="src/resources/student-card.jpg"
                        title="Marvel Student"
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h6" component="h2">
                            Student Login
                        </Typography>
                        <div align="left">
                            <form className="loginClass" noValidate autoComplete="off">
                                <TextField
                                    variant="outlined"
                                    id="stud-userName" type="email"
                                    name="userName" required="true"
                                    label="User Name" autoComplete="off"
                                    value={this.state.loginDetails.userName}
                                    onChange={this.handleOnChange()}
                                />
                                <TextField
                                    variant="outlined"
                                    id="stud-password"
                                    name="userPassword" required="true"
                                    type="password"
                                    label="Password" autoComplete="off"
                                    value={this.state.loginDetails.userPassword}
                                    onChange={this.handleOnChange()}
                                />
                                {<h4 className="error"> {this.state.errorMessage} </h4>}
                            </form>
                        </div>
                    </CardContent>
                </CardActionArea>
                <CardActions>
                    <center>
                        <Button size="small" color="primary" onClick={onLogin}>
                            Login
                        </Button>
                        <Button size="small" color="primary" onClick={goRegister}>
                            Signup
                        </Button>
                    </center>
                </CardActions>
                <CardActions>
                    <center>
                        <Button size="small" color="primary" onClick={onLoginFacebook}
                                startIcon={<Avatar src={'src/components/images/icons8-facebook-30.png'}/>}
                        />
                        <Button size="small" color="primary" onClick={onLoginGoogle}
                                startIcon={<Avatar src={'src/components/images/icons8-google-30.png'}/>}
                        />
                    </center>
                </CardActions>
            </Card>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        studentLogin: (value) =>
            dispatch({
                type: STUDENT_LOGIN,
                payload: value
            })
    };
};

const mapStateToProps = (state) => ({
    value: state.loginDetails,
    isStudentLoginSuccessful: state.isStudentLoginSuccessful,
    studentLoginId: state.studentLoginId,
    studentLoginName: state.studentLoginName
})

const StudentLogin = connect(
    mapStateToProps,
    mapDispatchToProps
)(StudentsCard)


export default withRouter(StudentLogin);

