import React, {useEffect, useState} from 'react';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from "@material-ui/core/TextField";
import './login.css';
import {COURSE_PROVIDER_LOGIN} from "../../actions/ActionConstants";
import {connect} from "react-redux";
import {Redirect, withRouter} from "react-router-dom";

import {Auth, Hub} from 'aws-amplify';

class CourseProvidersCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loginDetailsCourse: {
                userName: '',
                userPassword: '',
                loginType: '',
                email: '',
                authCode: '',
                formType: 'signUp',
                moduleType: 'course',
                token: '',
            },
            errorMessage: '',
            isSubmitted: false,
            isCourseProviderLoginExist: false,
        }
    }

    async componentDidMount() {
        console.log("Check existing user session")

        // try {
        // Check existing user session
        const courseProviderUser = await Auth.currentAuthenticatedUser()
        const courseProviderSession = await Auth.currentSession();

        const userInfo = await Auth.currentUserInfo()
        const email = userInfo.attributes.email;

        if (this.state.loginDetailsCourse.moduleType === 'course' && email === 'pradeep.charism@gmail.com' &&
            courseProviderUser !== '' && courseProviderSession != null) {
            this.props.history.push('/courseProviderDashboard');
        }


        //     this.setState({login: true})
        //
        //     this.state.loginDetailsCourse.token = courseProviderSession.getAccessToken().getJwtToken();
        //
        //     console.log('Course Provider access token courseprovider is : ', courseProviderSession.getAccessToken().getJwtToken())
        //     console.log('Course Provider access token issue at is : ', courseProviderSession.getAccessToken().getIssuedAt())
        //     console.log('Course Provider access token expired is : ', courseProviderSession.getAccessToken().getExpiration())
        //
        //     console.log('Course Provider access token issue at datetime is : ', new Date(courseProviderSession.getAccessToken().getIssuedAt() * 1000).toLocaleString("en-US"))
        //     console.log('Course Provider access token expired datetime is : ', new Date(courseProviderSession.getAccessToken().getExpiration() * 1000).toLocaleString("en-US"))
        //     console.log('Course Provider is : ', courseProviderUser)
        // } catch (err) {
        //     this.setState({login: false})
        //     console.log('no user found from courseprovidercard', err)
        // }
    }

    handleOnChange() {
        return (e) => {
            e.preventDefault();
            const {name, value} = e.target;
            const loginDetailsCourse = this.state.loginDetailsCourse;
            loginDetailsCourse[name] = value;
            this.setState({loginDetailsCourse});
        };
    }

    async checkUserInDB() {
        try {
            console.log('prepare check user against db in course provider');

            const userInfo = await Auth.currentUserInfo()
            const email = userInfo.attributes.email;
            const username = userInfo.username;
            const loginType = username.substring(0, username.indexOf("_"));

            const courseProviderSession = await Auth.currentSession();
            this.state.loginDetailsCourse.token = courseProviderSession.getAccessToken().getJwtToken();

            this.state.loginDetailsCourse.userName = email;
            this.state.loginDetailsCourse.loginType = loginType;

            console.log('this.state.loginDetailsCourse.userName', email);
            console.log('this.state.loginDetailsCourse.loginType', loginType);

            await this.props.courseProviderLogin(this.state);
            console.log('after checking user in db');
        } catch (error) {
            this.setState({errorMessage: 'Invalid Username/Password', isSubmitted: false})
            console.log('error signing in', error);
        }
    }

    render() {
        const goRegister = (event) => {
            event.preventDefault();
            this.props.history.push('/registerCourseProvider');
        }

        const onLogin = async (event) => {
            event.preventDefault();
            try {
                if (this.state.loginDetailsCourse.userName === '' || this.state.loginDetailsCourse.userPassword === '') {
                    this.setState({errorMessage: 'UserName/Password cannot be empty', isSubmitted: false})
                    return;
                }

                if (this.state.loginDetailsCourse.userName !== '') {
                    const pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
                    if (!pattern.test(this.state.loginDetailsCourse.userName)) {
                        this.setState({errorMessage: 'UserName must a valid email address', isSubmitted: false})
                        return;
                    }
                }

                if(this.state.loginDetailsCourse.userPassword !== ''){
                    const isValid = /^[0-9a-zA-Z]+$/i.test(this.state.loginDetailsCourse.userPassword);
                    if (!isValid) {
                        this.setState({errorMessage: 'Username/Password has invalid characters', isSubmitted: false})
                        return;
                    }
                }

                console.log('prepare signing in Cognito');
                console.log('this.state.loginDetailsCourse.userName', this.state.loginDetailsCourse.userName);
                console.log('this.state.loginDetailsCourse.userPassword', this.state.loginDetailsCourse.userPassword);
                const user = await Auth.signIn(this.state.loginDetailsCourse.userName, this.state.loginDetailsCourse.userPassword);
                this.state.loginDetailsCourse.loginType = "custom";

                const session = await Auth.currentSession();
                this.state.loginDetailsCourse.token = session.getAccessToken().getJwtToken();

                this.setState({isCourseProviderLoginExist: true})
                console.log('successfully signing in Cognito');
            } catch (error) {
                this.setState({errorMessage: 'User doesnt exist', isCourseProviderLoginExist: false})
                console.log('no user in Cognito', error);
            }
            // }

            if (this.state.isCourseProviderLoginExist) {
                try {
                    console.log('prepare check user against db');
                    await this.props.courseProviderLogin(this.state);
                    console.log('after checking user in db');
                } catch (error) {
                    this.setState({errorMessage: 'Invalid Username/Password', isSubmitted: false})
                    console.log('error signing in', error);
                }
            }

            console.log('user exist in cognito and in db');
            console.log('aaa this.props.isCourseProviderLoginSuccessful', this.props.isCourseProviderLoginSuccessful);
            this.setState({isSubmitted: true})
        }

        // const onLoginFacebook = async (event) => {
        //     event.preventDefault();
        //
        //     try {
        //         console.log('prepare signing in Facebook course provider');
        //         const user = await Auth.federatedSignIn({provider: 'Facebook'});
        //         console.log('successfully signing in Facebook');
        //
        //         const session = await Auth.currentSession();
        //         this.state.loginDetailsCourse.token = session.getAccessToken().getJwtToken();
        //
        //         this.setState({isCourseProviderLoginExist: true})
        //     } catch (error) {
        //         this.setState({errorMessage: 'User doesnt exist in Facebook', isCourseProviderLoginExist: false})
        //         console.log('no user in Facebook', error);
        //     }
        //     this.setState({isSubmitted: true})
        // }
        //
        // const onLoginGoogle = async (event) => {
        //     event.preventDefault();
        //
        //     try {
        //         console.log('prepare signing in Google');
        //         const user = await Auth.federatedSignIn({provider: 'Google'});
        //         console.log('successfully signing in Google');
        //
        //         const session = await Auth.currentSession();
        //         this.state.loginDetailsCourse.token = session.getAccessToken().getJwtToken();
        //
        //         this.setState({isCourseProviderLoginExist: true})
        //         // }
        //     } catch (error) {
        //         this.setState({errorMessage: 'User doesnt exist in Google', isCourseProviderLoginExist: false})
        //         console.log('no user in Google', error);
        //     }
        //     this.setState({isSubmitted: true})
        // }

        // if (this.props.isCourseProviderLoginSuccessful === 'Success') {
        //     return <Redirect to='/courseProviderDashboard'/>;
        // }
        //
        // if (this.state.isSubmitted && this.props.isCourseProviderLoginSuccessful === "Invalid username/password") {
        //     this.setState({errorMessage: 'Invalid Username/Password', isSubmitted: false})
        // }

        if (this.state.isSubmitted && this.props.isCourseProviderLoginSuccessful === 'No current user') {
            // console.log('isSubmitted from iscourseProviderLoginSuccessful ', this.props.iscourseProviderLoginSuccessful);
            this.setState({errorMessage: 'No current user', isSubmitted: false})
        }

        if (this.state.loginDetailsCourse.moduleType === 'course' && this.state.isSubmitted && this.props.isCourseProviderLoginSuccessful === 'Success') {
            this.props.history.push('/courseProviderDashboard');
        }

        if (this.state.isSubmitted && this.props.isCourseProviderLoginSuccessful === "Invalid username/password") {
            // this.setState({isSubmitted: false})
            console.log('come to isCourseProviderLoginSuccessful', this.props.isCourseProviderLoginSuccessful);
            console.log('isSubmitted', this.state.isSubmitted);
            console.log('loginType', this.state.loginDetailsCourse.loginType);

            if (this.state.loginDetailsCourse.loginType === "custom") {
                this.setState({errorMessage: 'Invalid Username/Password', isSubmitted: false})
            } else {
                this.props.history.push('/registerCourseProvider');
            }
        }

        return (

            <Card className="loginClass">
                <CardActionArea>
                    <CardMedia
                        component="img"
                        alt="Course Provider"
                        height="150"
                        image="src/resources/course-provider-card.jpg"
                        title="Course Provider"
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h6" component="h2">
                            Course Provider Login
                        </Typography>
                        <div align="left">
                            <form className="loginClass" noValidate autoComplete="off">
                                <TextField
                                    variant="outlined"
                                    id="cp-userName"
                                    name="userName"
                                    label="User Name" autoComplete="off"
                                    value={this.state.loginDetailsCourse.userName}
                                    onChange={this.handleOnChange()}
                                />
                                <TextField
                                    variant="outlined"
                                    id="cp-userPassword"
                                    name="userPassword"
                                    type="password"
                                    label="Password" autoComplete="off"
                                    value={this.state.loginDetailsCourse.userPassword}
                                    onChange={this.handleOnChange()}
                                />
                                {<h4 className="error"> {this.state.errorMessage} </h4>}
                            </form>
                        </div>
                    </CardContent>
                </CardActionArea>
                <CardActions>
                    <center>
                        <Button size="small" color="primary" onClick={onLogin}>
                            Login
                        </Button>
                        <Button size="small" color="primary" onClick={goRegister}>
                            Signup
                        </Button>
                    </center>
                </CardActions>
                <CardActions/>
                <CardActions/>
                <CardActions/>
                <CardActions/>
            </Card>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        courseProviderLogin: (value) =>
            dispatch({
                type: COURSE_PROVIDER_LOGIN,
                payload: value
            })
    };
};

const mapStateToProps = (state) => ({
    value: state.loginDetailsCourse,
    isCourseProviderLoginSuccessful: state.isCourseProviderLoginSuccessful
})

const CourseProviderLogin = connect(
    mapStateToProps,
    mapDispatchToProps
)(CourseProvidersCard)


export default withRouter(CourseProviderLogin);