import React, {Component} from 'react';
import {connect} from 'react-redux'
import {Button, TextField} from "@material-ui/core";
import {Redirect} from "react-router-dom";

import { Auth, Hub } from 'aws-amplify';

export class CourseProviderConfirmation extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            user: {
                username: '',
                confirmCode: '',
                moduleType: 'course',
            },
            errorMessage: '',
            confirmed: ''
        };
    }

    handleOnChange() {
        return (e) => {
            e.preventDefault();
            const {name, value} = e.target;
            const user = this.state.user;
            user[name] = value;
            this.setState({user});
        };
    }

    render() {
        const submitForm = async (event) => {
            event.preventDefault();
            this.setState({errorMessage: ''})
            console.log('this.props.courseProviderRegistrationConfirmation:', this.props.courseProviderRegistrationConfirmation);
            try {
                await Auth.confirmSignUp(
                    this.props.courseProviderRegistrationConfirmation,
                    this.state.user.confirmCode
                );
            } catch (error) {
                this.setState({errorMessage: 'Invalid Confirmation Code', confirmed: false})
                console.log('error signing up:', error);
                // FIXME redirect error page
            }

            this.setState({confirmed: true});
        }

        if (this.state.confirmed) {
            return <Redirect to='/'/>
        }

        return (
            <div align="center">
                <br/>
                <span>Registration Status: {this.props.courseProviderRegistrationConfirmation}</span>
                {<h4 className="error"> { this.state.errorMessage } </h4> }
                <br/>
                <br/>
                <TextField
                    variant="outlined"
                    id="confirmCode"
                    name="confirmCode"
                    label="Confirmation Code"
                    fullWidth
                    value={this.state.user.confirmCode}
                    onChange={this.handleOnChange()}
                />
                <Button variant="contained" color="primary" onClick={ submitForm }>
                    Confirm Register
                </Button>
                {/*<Button variant="contained" color="primary" onClick={() => this.setState({confirmed: true})}>*/}
                {/*    OK*/}
                {/*</Button>*/}
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    courseProviderRegistrationConfirmation: state.courseProviderRegistrationConfirmation,
})

export default connect(mapStateToProps)(CourseProviderConfirmation);
