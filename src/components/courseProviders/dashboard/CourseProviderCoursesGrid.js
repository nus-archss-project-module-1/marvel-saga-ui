import * as React from 'react';
import {useEffect, useState} from 'react';
import {DataGrid} from '@material-ui/data-grid';
import {getApi} from "../../../api/ApiService";
import Typography from "@material-ui/core/Typography";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import {useSelector} from "react-redux";
import {Redirect} from "react-router-dom";
import urls from '../../../api/ApiEndpoints.json'

const columns = [
    {field: 'id', headerName: 'ID', width: 70},
    {field: 'name', headerName: 'Course Provider Name', width: 200},
    // {field: 'courseProviderName', headerName: 'Course Provider Name', width: 200},
    {field: 'category', headerName: 'Category', width: 130},
    {field: 'subCategory', headerName: 'Sub Category', width: 160},
    {field: 'ageGroup', headerName: 'Age group', width: 130},
    {field: 'level', headerName: 'Skill Level', width: 130},
    {field: 'location', headerName: 'Location', width: 130},
    {field: 'teacher', headerName: 'Teacher', width: 130},
    {field: 'duration', headerName: 'Duration hours', width: 125},
    {field: 'classSize', headerName: 'Class size', width: 125},
    {field: 'fee', headerName: 'Fees', type: 'number', width: 90},
    {field: 'slots', headerName: 'Time Slots', width: 90},
    {field: 'frequency', headerName: 'Frequency', width: 125},
];

const EnrollmentAction = (open, selectedCourse, handleClose, handleOnBackdropClick) => {

    return <Dialog open={open} onClose={handleClose}
                   aria-labelledby="form-dialog-title"
                   maxWidth="lg"
                   onBackdropClick={handleOnBackdropClick}
    >
        <DialogTitle id="form-dialog-title">Course Enrollment Form</DialogTitle>
        <DialogContent>
            <DialogContentText>
                Fill your course schedule details
            </DialogContentText>
            <table align="center">
                <tbody>
                <tr>
                    <td>
                        <TextField
                            variant="outlined"
                            id="enrol-name"
                            name="name"
                            label="Course Name"
                            value={selectedCourse.name}
                            readOnly
                        />
                    </td>
                    <td>
                        <TextField
                            variant="outlined"
                            id="enrol-courseProviderName"
                            name="courseProviderName"
                            label="Course Provider Name"
                            value={selectedCourse.courseProviderName}
                            readOnly
                        />
                    </td>
                </tr>
                <tr>
                    <td><br/>
                        <TextField
                            variant="outlined"
                            id="enrol-category"
                            name="category"
                            label="Course Category"
                            value={selectedCourse.category}
                            readOnly
                        />
                    </td>
                    <td><br/>
                        <TextField
                            variant="outlined"
                            id="enrol-subCategory"
                            name="subCategory"
                            label="Sub Category"
                            value={selectedCourse.subCategory}
                            readOnly
                        />
                    </td>
                </tr>
                <tr>
                    <td><br/>
                        <TextField
                            variant="outlined"
                            id="classSize"
                            name="classSize"
                            label="Class Size"
                            value={selectedCourse.classSize}
                            readOnly
                        />
                    </td>
                    <td><br/>
                        <TextField
                            id="enrol-ageGroup"
                            name="ageGroup"
                            label="ageGroup"
                            variant="outlined"
                            value={selectedCourse.ageGroup}
                            readOnly
                        />
                    </td>
                </tr>
                <tr>
                    <td><br/>
                        <TextField
                            variant="outlined"
                            id="grades"
                            name="grades"
                            label="Grades"
                            value={selectedCourse.grades}
                            readOnly
                        />
                    </td>
                    <td><br/>
                        <TextField
                            variant="outlined"
                            id="level"
                            name="level"
                            label="Skill Level"
                            value={selectedCourse.level}
                            readOnly
                        />
                    </td>
                </tr>
                <tr>
                    <td><br/>
                        <TextField
                            variant="outlined"
                            id="teacher"
                            name="teacher"
                            label="Teacher"
                            value={selectedCourse.teacher}
                            readOnly
                        />
                    </td>
                    <td><br/>
                        <TextField
                            variant="outlined"
                            id="frequency"
                            name="frequency"
                            label="Frequency"

                            value={selectedCourse.frequency}
                            readOnly
                        />
                    </td>
                </tr>
                <tr>
                    <td><br/>
                        <TextField
                            variant="outlined"
                            id="duration"
                            name="duration"
                            label="Duration"

                            value={selectedCourse.duration}
                            readOnly
                        />
                    </td>
                    <td><br/>
                        <TextField
                            variant="outlined"
                            id="fee"
                            name="fee"
                            label="Fee"

                            value={selectedCourse.fee}
                            readOnly
                        />
                    </td>
                    <td/>
                </tr>
                </tbody>
            </table>
        </DialogContent>
    </Dialog>;
}

export default function CourseProviderCoursesGrid(props) {
    const [courseList, setCourseList] = useState([]);
    const [selectedCourse, setSelectedCourse] = useState({});
    const [action, setAction] = useState(false);
    const [rowSelected, setRowSelected] = useState(false);

    const [open, setOpen] = React.useState(true);
    const [isEnrolled, setIsEnrolled] = React.useState(false);

    const courseProviderLoginId = useSelector(state => state.courseProviderLoginId)
    // const studentLoginName = useSelector(state => state.studentLoginName)

    const handleClose = () => {
        setOpen(false);
    };

    const handleOnBackdropClick = () => {
        console.log("onBackdropClick")
        setAction(false)
        setRowSelected(false)
    }

    useEffect(() => {
        console.log("Fetching my courses for Course Provider id: ", courseProviderLoginId)
        getApi(urls[process.env.URL_CONFIG].courseProviderUrl + '/marvel-course-provider/fetchCourseProviderCourses/' + courseProviderLoginId)
            .then(response => {
                setCourseList(response.data)
            })
            .catch(error => {
                console.log("Error occurred: ", error.data)
            });

    }, [])

    useEffect(() => {
        console.log("useEffect ", action)
        if (rowSelected) {
            setAction(true)
        }
    }, [selectedCourse])

    const handleRowSelected = (params) => {
        console.log(params)
        if (params !== "undefined") {
            setSelectedCourse(params.row)
            setRowSelected(true);
            setAction(true);
            setOpen(true);
            console.log("handleRowSelected ", action)
        }
    }

    if (isEnrolled) {
        return <Redirect to='/studentDashboard'/>;
    }

    return (
        <div style={{height: 400, width: '100%'}}>
            <Typography align="center" gutterBottom variant="h6" component="h2">
                Course catalog
            </Typography>

            {action === true && EnrollmentAction(open, selectedCourse, handleClose, handleOnBackdropClick)}
            <DataGrid rows={courseList} columns={columns}
                      density="standard"
                      pageSize={5}
                      disableMultipleSelection={true}
                      onRowDoubleClick={handleRowSelected}
            />

            <Typography gutterBottom align='right' color="primary" variant="button">
                Double click the row view course details.
            </Typography>
        </div>
    )
        ;
}
