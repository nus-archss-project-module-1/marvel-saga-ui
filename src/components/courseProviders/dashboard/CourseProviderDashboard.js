import React, {useEffect, useState} from 'react';
import clsx from 'clsx';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import Box from '@material-ui/core/Box';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import NotificationsIcon from '@material-ui/icons/Notifications';
import {DashboardMenu} from './CourseProviderMenuItems';
import Chart from './Chart';
import Deposits from './Deposits';
import {useHistory, withRouter} from "react-router-dom";
import RegisterCourse from "../../courses/CourseRegistration";
import Copyright from "../../common/CopyRight";
import CourseProviderCoursesGrid from "./CourseProviderCoursesGrid";

import Style from "./Style";
import CourseProviderStudentsGrid from "./CourseProviderStudentsGrid";

import SessionTimeout from '../../common/SessionTimeout';

import { Auth, Hub } from 'aws-amplify';

const useStyles = Style();


function DashboardAppBar(open, handleDrawerOpen) {
    const classes = useStyles();

    return <AppBar position="absolute" className={clsx(classes.appBar, open && classes.appBarShift)}>
        <Toolbar className={classes.toolbar}>
            <IconButton
                edge="start"
                color="inherit"
                aria-label="open drawer"
                onClick={handleDrawerOpen}
                className={clsx(classes.menuButton, open && classes.menuButtonHidden)}
            >
                <MenuIcon/>
            </IconButton>
            <Typography align="center" component="h1" variant="h6" color="inherit" noWrap
                        className={classes.title}>
                Marvel Course Provider Dashboard
            </Typography>
            <IconButton color="inherit">
                <Badge badgeContent={4} color="secondary">
                    <NotificationsIcon/>
                </Badge>
            </IconButton>
        </Toolbar>
    </AppBar>;
}

export function CourseProviderDashboard() {
    let history = useHistory();
    const classes = useStyles();
    const [open, setOpen] = useState(true);
    const [showNewCourse, setShowNewCourse] = useState(false);
    const [showReports, setShowReports] = useState(false);
    const [showCourses, setShowCourses] = useState(false);
    const [showStudents, setShowStudents] = useState(false);
    const [showDashboard, setShowDashboard] = useState(true);
    const [logout, setLogout] = useState(false);
    const [isCourseProviderAuthenticated, setCourseProviderAuthenticated] = useState(false);

    const resetDisplay = () => {
        setShowReports(false);
        setShowNewCourse(false);
        setShowCourses(false);
        setShowStudents(false);
        setShowDashboard(false);
    };

    const handleNewCourse = () => {
        resetDisplay();
        setShowNewCourse(true);
    };
    const handleDashboard = () => {
        resetDisplay();
        setShowReports(true);
        setShowNewCourse(false);
        setShowCourses(true);
        setShowStudents(false);
    };

    const handleCourses = () => {
        resetDisplay();
        setShowCourses(true);
    }
    const handleStudents = () => {
        resetDisplay();
        setShowStudents(true);
    };

    const handleLogout = async () => {
        console.log('log out click');
        try {
            // Sign out user
            await Auth.signOut();
        } catch(err) {
            console.log('no user found', err)
        }
        resetDisplay();
        setLogout(true);
    };

    const handleDrawerOpen = () => {
        setOpen(true);
    };
    const handleDrawerClose = () => {
        setOpen(false);
    };
    const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);

    const updateTimeout = async() => {
        const session = await Auth.currentSession();
        const issuedAt = session.getAccessToken().getIssuedAt()
        const expiration = session.getAccessToken().getExpiration()
        let timeout = expiration - issuedAt

        // this.setState({ sessionTimeout: timeout }, () => {
        //     console.log(this.state.sessionTimeout, 'sessionTimeout');
        // });

        // setSessionTimeout(timeout)

        // console.log('user access token is : ', session.getAccessToken().getJwtToken())
        // console.log('user access token issue at is : ', issuedAt)
        // console.log('user access token expired is : ', expiration)
        // console.log('timeout is : ', timeout);

        return timeout
    };

    useEffect(async () => {
        console.log("Course provider Logging out.... ", logout)
        // if (logout) {
        //     history.push('/logout');
        // }
        // setLogout(false);

        try {
            // Check existing user after logout
            const courseProviderUser = await Auth.currentAuthenticatedUser()

            const courseProviderSession = await Auth.currentSession();

            if( courseProviderUser === '' && courseProviderSession === null){
                history.push('/');
            }

            setCourseProviderAuthenticated(true);
            console.log('Course Provider user is : ', courseProviderUser)

            // const authOption = await Auth.configure()
            // console.log('authOption is : ', authOption)

        } catch(err) {
            setCourseProviderAuthenticated(false);
            setLogout(true);

            //history.push('/');
            console.log('no user found from courseProviderdashboard', err)
        }

        if (logout) {
            console.log('logout true')
            setLogout(true);
            history.push('/');
            // history.push('/logout');
        }
        setLogout(false);
    }, [logout])

    return (
        <div className={classes.root}>
            <SessionTimeout isAuthenticated={isCourseProviderAuthenticated} logOut={handleLogout} />
            <CssBaseline/>
            {DashboardAppBar(open, handleDrawerOpen)}
            <Drawer
                variant="permanent"
                classes={{
                    paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
                }}
                open={open}
            >
                <div className={classes.toolbarIcon}>
                    <IconButton onClick={handleDrawerClose}>
                        <ChevronLeftIcon/>
                    </IconButton>
                </div>
                <Divider/>
                <List>{DashboardMenu(handleDashboard, handleNewCourse, handleCourses, handleStudents, handleLogout)}</List>
                <Divider/>
            </Drawer>
            <main className={classes.content}>
                <div className={classes.appBarSpacer}/>
                <Container maxWidth="xl" className={classes.container}>
                    {/* Dashboard*/}
                    {showDashboard ?
                        <Grid item xs={12}>
                            <Paper className={classes.paper}>
                                <CourseProviderCoursesGrid/>
                            </Paper>
                        </Grid>
                        : ''}

                    {/* New Course */}
                    <Grid container spacing={3}>
                        {showNewCourse ?
                            <Grid item xs={12}>
                                <RegisterCourse/>
                            </Grid>
                            : ''}

                        {/* Reports */}
                        {showReports ?
                            <Grid item xs={12} md={8} lg={9}>
                                <Paper className={fixedHeightPaper}>
                                    <Chart/>
                                </Paper>
                            </Grid> : ''}

                        {/* Students */}
                        {showStudents ?
                            <Grid item xs={12}>
                                <CourseProviderStudentsGrid/>
                            </Grid> : ''}

                        {/* Courses */}
                        {showCourses ?
                            <Grid item xs={12}>
                                <Paper>
                                    <CourseProviderCoursesGrid/>
                                </Paper>
                            </Grid>
                            : ''}
                    </Grid>
                    <Box pt={4}>
                        <Copyright/>
                    </Box>
                </Container>
            </main>
        </div>
    );
}

export default withRouter(CourseProviderDashboard)