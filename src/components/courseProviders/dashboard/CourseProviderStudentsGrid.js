import * as React from 'react';
import {useEffect, useState} from 'react';
import {DataGrid} from '@material-ui/data-grid';
import {getApi} from "../../../api/ApiService";
import Typography from "@material-ui/core/Typography";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import {useSelector} from "react-redux";
import {Redirect} from "react-router-dom";
import urls from '../../../api/ApiEndpoints.json'

const columns = [
    {field: 'id', headerName: 'ID', width: 70},
    {field: 'studentId', headerName: 'Student Id', width: 160},
    {field: 'studentName', headerName: 'Student Name', width: 130},
    {field: 'gender', headerName: 'Gender', width: 130},
    {field: 'parentName', headerName: 'Parent Name', width: 130},
    {field: 'contactNumber', headerName: 'Contact Number', width: 130},
    {field: 'email', headerName: 'Email', width: 125},
    {field: 'name', headerName: 'Course Name', width: 200},
    {field: 'courseId', headerName: 'Course Id', width: 130},
    {field: 'teacher', headerName: 'Teacher', width: 125},
];

const EnrollmentAction = (open, selectedCourse, handleClose, handleOnBackdropClick) => {

    return <Dialog open={open} onClose={handleClose}
                   aria-labelledby="form-dialog-title"
                   maxWidth="lg"
                   onBackdropClick={handleOnBackdropClick}
    >
        <DialogTitle id="form-dialog-title">Student Particulars</DialogTitle>
        <DialogContent>
            <DialogContentText>
                Student Information
            </DialogContentText>
            <table align="center">
                <tbody>
                <tr>
                    <td>
                        <TextField
                            variant="outlined"
                            id="enrol-studentId"
                            name="studentId"
                            label="Student Id"
                            value={selectedCourse.studentId}
                            readOnly
                        />
                    </td>
                    <td>
                        <TextField
                            variant="outlined"
                            id="enrol-studentName"
                            name="studentName"
                            label="Student Name"
                            value={selectedCourse.studentName}
                            readOnly
                        />
                    </td>
                </tr>
                <tr>
                    <td><br/>
                        <TextField
                            variant="outlined"
                            id="enrol-gender"
                            name="gender"
                            label="Gender"
                            value={selectedCourse.gender}
                            readOnly
                        />
                    </td>
                    <td><br/>
                        <TextField
                            variant="outlined"
                            id="enrol-parentName"
                            name="parentName"
                            label="Parent Name"
                            value={selectedCourse.parentName}
                            readOnly
                        />
                    </td>
                </tr>
                <tr>
                    <td><br/>
                        <TextField
                            variant="outlined"
                            id="contactNumber"
                            name="contactNumber"
                            label="Contact Number"
                            value={selectedCourse.contactNumber}
                            readOnly
                        />
                    </td>
                    <td><br/>
                        <TextField
                            id="email-id"
                            name="email"
                            label="Email"
                            variant="outlined"
                            value={selectedCourse.email}
                            readOnly
                        />
                    </td>
                </tr>
                <tr>
                    <td><br/>
                        <TextField
                            variant="outlined"
                            id="course-name"
                            name="name"
                            label="Course Name"
                            value={selectedCourse.name}
                            readOnly
                        />
                    </td>
                    <td><br/>
                        <TextField
                            variant="outlined"
                            id="courseId"
                            name="courseId"
                            label="Course Id"
                            value={selectedCourse.courseId}
                            readOnly
                        />
                    </td>
                </tr>
                <tr>
                    <td><br/>
                        <TextField
                            variant="outlined"
                            id="teacher"
                            name="teacher"
                            label="Teacher"
                            value={selectedCourse.teacher}
                            readOnly
                        />
                        <br/>
                    </td>
                    <td/>
                </tr>
                </tbody>
            </table>
        </DialogContent>
    </Dialog>;
}

export default function CourseProviderStudentsGrid(props) {
    const [courseStudentsList, setCourseStudentsList] = useState([]);
    const [selectedCourse, setSelectedCourse] = useState({});
    const [action, setAction] = useState(false);
    const [rowSelected, setRowSelected] = useState(false);

    const [open, setOpen] = React.useState(true);
    const [isEnrolled, setIsEnrolled] = React.useState(false);

    const courseProviderLoginId = useSelector(state => state.courseProviderLoginId)
    // const studentLoginName = useSelector(state => state.studentLoginName)

    const handleClose = () => {
        setOpen(false);
    };

    const handleOnBackdropClick = () => {
        console.log("onBackdropClick")
        setAction(false)
        setRowSelected(false)
    }

    useEffect(() => {
        console.log("Fetching my courses for Course Provider id: ", courseProviderLoginId)
        getApi(urls[process.env.URL_CONFIG].courseProviderUrl + '/marvel-course-provider/fetchCourseStudents/' + courseProviderLoginId)
            .then(response => {
                setCourseStudentsList(response.data)
            })
            .catch(error => {
                console.log("Error occurred: ", error.data)
            });

    }, [])

    useEffect(() => {
        console.log("useEffect ", action)
        if (rowSelected) {
            setAction(true)
        }
    }, [selectedCourse])

    const handleRowSelected = (params) => {
        console.log(params)
        if (params !== "undefined") {
            setSelectedCourse(params.row)
            setRowSelected(true);
            setAction(true);
            setOpen(true);
            console.log("handleRowSelected ", action)
        }
    }

    if (isEnrolled) {
        return <Redirect to='/courseProviderDashboard'/>;
    }

    return (
        <div style={{height: 400, width: '100%'}}>
            <Typography align="center" gutterBottom variant="h6" component="h2">
                Students enrolled with the Course Provider
            </Typography>

            {action === true && EnrollmentAction(open, selectedCourse, handleClose, handleOnBackdropClick)}
            <DataGrid rows={courseStudentsList} columns={columns}
                      density="standard"
                      pageSize={5}
                      disableMultipleSelection={true}
                      onRowDoubleClick={handleRowSelected}
            />

            <Typography gutterBottom align='right' color="primary" variant="button">
                Double click the row view course details.
            </Typography>
        </div>
    )
        ;
}
