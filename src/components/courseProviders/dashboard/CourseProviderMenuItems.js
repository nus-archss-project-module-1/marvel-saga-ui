import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import DashboardIcon from '@material-ui/icons/Dashboard';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import PeopleIcon from '@material-ui/icons/People';
import BarChartIcon from '@material-ui/icons/BarChart';
import LayersIcon from '@material-ui/icons/Layers';
import AssignmentIcon from '@material-ui/icons/Assignment';
import SchoolIcon from '@material-ui/icons/School';
import Divider from "@material-ui/core/Divider";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";

export function loadNewCourse() {
    console.log("Load New Course")
    this.props.history.push("/registerCourse")
}

export function DashboardMenu(handleDashboard, handleNewCourse, handleCourses, handleStudents, handleLogout) {
    return <div>
        <ListItem button>
            <ListItemIcon>
                <DashboardIcon/>
            </ListItemIcon>
            <ListItemText primary="Dashboard" onClick={handleDashboard}/>
        </ListItem>
        <ListItem button>
            <ListItemIcon>
                <ShoppingCartIcon/>
            </ListItemIcon>
            <ListItemText primary="New Course" onClick={handleNewCourse}/>
        </ListItem>
        <ListItem button>
            <ListItemIcon>
                <SchoolIcon/>
            </ListItemIcon>
            <ListItemText primary="Courses" onClick={handleCourses}/>
        </ListItem>
        <ListItem button>
            <ListItemIcon>
                <PeopleIcon/>
            </ListItemIcon>
            <ListItemText primary="Students" onClick={handleStudents}/>
        </ListItem>
        <Divider/>
        <ListItem button>
            <ListItemIcon>
                <ExitToAppIcon/>
            </ListItemIcon>
            <ListItemText primary="Log out" onClick={handleLogout}/>
        </ListItem>
    </div>;
}