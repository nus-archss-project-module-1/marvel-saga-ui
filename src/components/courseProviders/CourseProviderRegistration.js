import React, {Component} from 'react';
import {Button, Container, CssBaseline, Paper, TextField} from "@material-ui/core";
import {courseProviderRegistered} from "../../actions";
import {connect} from "react-redux";
import {REGISTER_COURSE_PROVIDER} from "../../actions/ActionConstants";
import Typography from "@material-ui/core/Typography";
import {Redirect} from 'react-router-dom';

import {Auth} from 'aws-amplify';

class CourseProviderRegistration extends Component {
    constructor(props) {
        super(props);
        this.state = this.initialize()
    }

    initialize() {
        return {
            courseProvider: {
                name: '',
                licenseNumber: '',
                licenseDocument: '',
                contactNumber: '',
                email: '',
                address: '',
                district: '',
                country: '',
                postalCode: '',
                userPassword: '',
                confirmUserPassword: '',
                loginType: 'custom',
                moduleType: 'course',
                token: '',
            },
            errors: {
                courseProvider: {
                    name: 'Enter Student Name',
                    licenseNumber: 'Enter Parent Name',
                    contactNumber: 'Enter Contact Number',
                    email: 'Email Valid Email',
                    address: 'Enter Address',
                    district: 'Enter District',
                    country: 'Enter Country',
                    postalCode: 'Enter Postal Code',
                }
            },
            validForm: false,
            submitted: false,
            isLoading: false,
            hasErrored: false,
            cancelled: false,
        };
    }

    async componentDidMount() {

        try {
            // Check existing user after logout
            const userInfo = await Auth.currentUserInfo()

            const user = this.state.courseProvider;
            user.studentName = userInfo.attributes.name;
            user.email = userInfo.attributes.email;

            const username = userInfo.username;
            user.loginType = username.substring(0, username.indexOf("_"));

            const session = await Auth.currentSession();
            user.token = session.getAccessToken().getJwtToken();

            console.log('user state', user);

            this.setState({user});

        } catch (err) {
            console.log('no user found from courseProviderregistration', err)
        }

    }

    handleOnChange() {
        return (e) => {
            e.preventDefault();
            const {name, value} = e.target;
            const courseProvider = this.state.courseProvider;
            courseProvider[name] = value;
            this.setState({courseProvider});
        };
    }

    render() {

        const submitForm = async (event) => {
            event.preventDefault();
            console.log(this.state.courseProvider);
            console.log('this.state.courseProvider.loginType:', this.state.courseProvider.loginType);
            this.props.registerCourseProvider(this.state);

            if (this.state.courseProvider.loginType === "custom") {
                try {
                    await Auth.signUp({
                        username: this.state.courseProvider.email,
                        password: this.state.courseProvider.userPassword,
                        attributes: {email: this.state.courseProvider.email}
                    });
                } catch (error) {
                    console.log('error signing up:', error);
                }
            }

            this.setState({submitted: true});
        }

        const cancelForm = async (event) => {
            event.preventDefault();
            try {
                // Sign out user
                await Auth.signOut();
            } catch (err) {
                console.log('no user found', err)
            }
            this.setState({cancelled: true});
            this.setState({submitted: true});
        }

        // if (this.state.submitted) {
        //     return <Redirect to='/confirmCourseProvider'/>
        // }

        if (this.state.submitted && this.state.cancelled) {
            return <Redirect to='/'/>
        }

        if (this.state.submitted && this.state.courseProvider.loginType === "custom") {
            console.log('this.state.submitted:', this.state.submitted);
            return <Redirect to='/confirmCourseProvider'/>
        }

        if (this.state.submitted && this.state.courseProvider.loginType !== "custom") {
            return <Redirect to='/courseProviderDashboard'/>
        }

        const resetForm = (event) => {
            event.preventDefault();
            this.setState({
                courseProvider: {
                    name: '',
                    licenseNumber: '',
                    licenseDocument: '',
                    contactNumber: '',
                    email: '',
                    address: '',
                    district: '',
                    country: '',
                    loginType: 'custom',
                    moduleType: 'course',
                    token: '',
                }
            });
            console.log(this.state.courseProvider)
        }


        return (
            <React.Fragment>
                <CssBaseline/>

                <Container maxWidth="md">
                    <Paper>
                        <br/>
                        <Typography align="center" gutterBottom variant="h6" component="h2">
                            Course Provider Registration Form
                        </Typography>
                        <form noValidate autoComplete="off">
                            <table align="center">
                                <tbody>
                                <tr>
                                    <td>
                                        <TextField
                                            variant="outlined"
                                            id="name"
                                            name="name"
                                            label="Course Provider Name"
                                            value={this.state.courseProvider.name}
                                            onChange={this.handleOnChange()}
                                        />
                                    </td>
                                    <td>
                                        <TextField
                                            variant="outlined"
                                            id="licenseNumber"
                                            name="licenseNumber"
                                            label="License Number"
                                            value={this.state.courseProvider.licenseNumber}
                                            onChange={this.handleOnChange()}
                                        />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <TextField
                                            variant="outlined"
                                            id="email"
                                            name="email"
                                            label="Email"
                                            fullWidth
                                            value={this.state.courseProvider.email}
                                            onChange={this.handleOnChange()}
                                        />
                                    </td>
                                    <td>
                                        <TextField
                                            variant="outlined"
                                            id="contactNumber"
                                            name="contactNumber"
                                            label="Contact Number"
                                            value={this.state.courseProvider.contactNumber}
                                            fullWidth
                                            onChange={this.handleOnChange()}
                                        />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <TextField
                                            id="address"
                                            name="address"
                                            label="Address"
                                            multiline
                                            fullWidth
                                            variant="outlined"
                                            value={this.state.courseProvider.address}
                                            onChange={this.handleOnChange()}
                                        />
                                    </td>
                                    <td>
                                        <TextField
                                            variant="outlined"
                                            id="district"
                                            name="district"
                                            label="District"
                                            fullWidth
                                            value={this.state.courseProvider.district}
                                            onChange={this.handleOnChange()}
                                        />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <TextField
                                            variant="outlined"
                                            id="country"
                                            name="country"
                                            label="Country"
                                            fullWidth
                                            value={this.state.courseProvider.country}
                                            onChange={this.handleOnChange()}
                                        />
                                    </td>
                                    <td>
                                        <TextField
                                            variant="outlined"
                                            id="postalCode"
                                            name="postalCode"
                                            label="Postal Code"
                                            fullWidth
                                            value={this.state.courseProvider.postalCode}
                                            onChange={this.handleOnChange()}
                                        />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <TextField
                                            variant="outlined"
                                            id="userPassword"
                                            type="password"
                                            name="userPassword"
                                            label="Password"
                                            fullWidth autoComplete="off"
                                            value={this.state.courseProvider.userPassword}
                                            onChange={this.handleOnChange()}
                                        />
                                    </td>
                                    <td>
                                        <TextField
                                            variant="outlined"
                                            type="password"
                                            id="confirmUserPassword"
                                            name="confirmUserPassword"
                                            label="Confirm Password"
                                            fullWidth autoComplete="off"
                                            value={this.state.courseProvider.confirmUserPassword}
                                            onChange={this.handleOnChange()}
                                        />
                                    </td>
                                </tr>
                                </tbody>
                                <tfoot align="center">
                                <tr>
                                    <td>
                                        <Button variant="contained" color="primary" onClick={submitForm}>
                                            Submit
                                        </Button>
                                    </td>
                                    <td>
                                        <Button variant="contained" color="primary" onClick={resetForm}>
                                            Clear
                                        </Button>
                                    </td>
                                    <td>
                                        <br/>
                                        <Button variant="contained" color="primary" onClick={cancelForm}>
                                            Cancel
                                        </Button>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </form>
                        <br/>
                    </Paper>
                </Container>
            </React.Fragment>

        )
    }
}


const mapDispatchToProps = dispatch => {
    return {
        isRegistered: courseProviderRegistered,
        registerCourseProvider: (value) =>
            dispatch({
                type: REGISTER_COURSE_PROVIDER,
                payload: value
            })
    };
};

const mapStateToProps = (state) => ({
    value: state.courseProvider
})

const RegisterCourseProvider = connect(
    mapStateToProps,
    mapDispatchToProps
)(CourseProviderRegistration)


export default RegisterCourseProvider;