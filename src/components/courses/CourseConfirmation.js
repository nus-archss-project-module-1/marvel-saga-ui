import React, {Component} from 'react';
import {connect} from 'react-redux'
import {Button} from "@material-ui/core";
import {Redirect} from "react-router-dom";


export class CourseConfirmation extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            confirmed: ''
        };
    }

    render() {
        if (this.state.confirmed) {
            return <Redirect to='/courseProviderDashboard'/>
        }
        return (
            <div align="center">
                <br/>
                <span>Registration Status: {this.props.courseRegistrationConfirmation}</span>
                <br/>
                <br/>
                <Button variant="contained" color="primary" onClick={() => this.setState({confirmed: true})}>
                    OK
                </Button>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    courseRegistrationConfirmation: state.courseRegistrationConfirmation,
})

export default connect(mapStateToProps)(CourseConfirmation);
