import React, {Component} from 'react';
import {Button, Container, CssBaseline, Paper, TextField} from "@material-ui/core";
import {connect} from "react-redux";
import {REGISTER_COURSE} from "../../actions/ActionConstants";
import Typography from "@material-ui/core/Typography";
import {courseRegistered} from "../../actions";
import {Redirect} from "react-router-dom";
import {COURSE_CATEGORY, COURSE_SUB_CATEGORY} from "../common/CourseConstants";
import Autocomplete from "@material-ui/lab/Autocomplete";


class CourseRegistration extends Component {
    constructor(props) {
        super(props);
        this.state = this.initialize()
    }

    initialize() {
        return {
            course: {
                name: '',
                category: '',
                subCategory: '',
                classSize: '',
                ageGroup: '',
                level: '',
                teacher: '',
                duration: '',
                fee: '',
                startDate: '',
                endDate: ''
            },
            errors: {
                course: {
                    name: 'Enter Course Name',
                    category: 'Enter Course Category',
                    subCategory: 'Enter Course Sub-Category',
                    classSize: 'Enter Class Size',
                    ageGroup: 'Enter Age-Group',
                    grades: 'Enter Grades',
                    teacher: 'Enter Teacher for the Course',
                    fee: 'Enter Fee Structure',
                }
            },
            validForm: false,
            submitted: false,
            isLoading: false,
            hasErrored: false

        };
    }

    handleOnChange() {
        return (e) => {
            e.preventDefault();
            const {name, value} = e.target;
            const course = this.state.course;
            course[name] = value;
            this.setState({course});
        };
    }

    render() {

        const submitForm = async (event) => {
            event.preventDefault();
            console.log("New Course courseProviderId:", this.props.courseProviderId)
            console.log("New Course courseProviderName:", this.props.courseProviderName)

            const course = this.state.course;
            course['courseProviderId'] = this.props.courseProviderId;
            this.setState({course});

            this.props.registerCourse(this.state);
            this.setState({submitted: true});
        }

        if (this.state.submitted) {
            return <Redirect to='/confirmCourse'/>
        }

        const resetForm = (event) => {
            event.preventDefault();
            this.setState({
                course: {
                    name: '',
                    category: '',
                    subCategory: '',
                    classSize: '',
                    ageGroup: '',
                    grades: '',
                    preRequisite: '',
                    teacher: '',
                    location: '',
                    duration: '',
                    fee: '',
                    frequency: '',
                }
            });
            console.log(this.state.course)
        }

        return (
            <React.Fragment>
                <CssBaseline/>

                <Container maxWidth="md">
                    <Paper>
                        <br/>
                        <Typography align="center" gutterBottom variant="h6" component="h2">
                            Course Registration Form
                        </Typography>
                        <table align="center">
                            <tbody>
                            <tr>
                                <td>
                                    <TextField
                                        variant="outlined"
                                        id="name"
                                        name="name"
                                        label="Course Name"
                                        value={this.state.course.name}
                                        onChange={this.handleOnChange()}
                                    />
                                </td>
                                <td>
                                    <Autocomplete
                                        id="course-category"
                                        options={COURSE_CATEGORY}
                                        getOptionLabel={(option) => option.title}
                                        renderInput={(params) =>
                                            <TextField {...params}
                                                       id="category"
                                                       name="category"
                                                       label="Course Category"
                                                       value={this.state.course.category}
                                                       onSelect={this.handleOnChange()}
                                                       variant="outlined"/>}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td><br/>
                                    <Autocomplete
                                        id="sub-category"
                                        options={COURSE_SUB_CATEGORY}
                                        getOptionLabel={(option) => option.title}
                                        // style={{width: 300}}
                                        renderInput={(params) =>
                                            <TextField {...params}
                                                       id="subcategory"
                                                       name="subCategory"
                                                       label="Sub Category"
                                                       fullWidth
                                                       value={this.state.course.subCategory}
                                                       onSelect={this.handleOnChange()}
                                                       variant="outlined"/>}
                                    />
                                </td>
                                <td><br/>
                                    <TextField
                                        variant="outlined"
                                        id="classSize"
                                        name="classSize"
                                        label="Class Size"
                                        value={this.state.course.classSize}
                                        fullWidth
                                        onChange={this.handleOnChange()}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td><br/>
                                    <TextField
                                        id="ageGroup"
                                        name="ageGroup"
                                        label="ageGroup"
                                        multiline
                                        fullWidth
                                        variant="outlined"
                                        value={this.state.course.ageGroup}
                                        onChange={this.handleOnChange()}
                                    />
                                </td>
                                <td><br/>
                                    <TextField
                                        variant="outlined"
                                        id="grades"
                                        name="grades"
                                        label="Grades"
                                        fullWidth
                                        value={this.state.course.grades}
                                        onChange={this.handleOnChange()}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td><br/>
                                    <TextField
                                        variant="outlined"
                                        id="level"
                                        name="level"
                                        label="Skill Level"
                                        fullWidth
                                        value={this.state.course.level}
                                        onChange={this.handleOnChange()}
                                    />
                                </td>
                                <td><br/>
                                    <TextField
                                        variant="outlined"
                                        id="teacher"
                                        name="teacher"
                                        label="Teacher"
                                        fullWidth
                                        value={this.state.course.teacher}
                                        onChange={this.handleOnChange()}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td><br/>
                                    <TextField
                                        variant="outlined"
                                        id="location"
                                        name="location"
                                        label="location"
                                        fullWidth
                                        value={this.state.course.location}
                                        onChange={this.handleOnChange()}
                                    />
                                </td>
                                <td><br/>
                                    <TextField
                                        variant="outlined"
                                        id="duration"
                                        name="duration"
                                        label="Duration"
                                        fullWidth
                                        type="number"
                                        value={this.state.course.duration}
                                        onChange={this.handleOnChange()}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td><br/>
                                    <TextField
                                        variant="outlined"
                                        id="fee"
                                        name="fee"
                                        label="Fee"
                                        fullWidth
                                        value={this.state.course.fee}
                                        onChange={this.handleOnChange()}
                                    />
                                </td>
                                <td><br/>
                                    <TextField
                                        variant="outlined"
                                        id="frequency"
                                        name="frequency"
                                        label="Frequency"
                                        fullWidth
                                        value={this.state.course.frequency}
                                        onChange={this.handleOnChange()}
                                    />
                                </td>
                            </tr>
                            </tbody>
                            <tfoot align="center">
                            <tr>
                                <td><br/>
                                    <Button variant="contained" color="primary" onClick={submitForm}>
                                        Submit
                                    </Button>
                                </td>
                                <td><br/>
                                    <Button variant="contained" color="primary" onClick={resetForm}>
                                        Clear
                                    </Button>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                        <br/>
                    </Paper>
                </Container>
            </React.Fragment>

        )
    }
}


const mapDispatchToProps = dispatch => {
    return {
        isRegistered: courseRegistered,
        registerCourse: (value) =>
            dispatch({
                type: REGISTER_COURSE,
                payload: value
            })
    };
};

const mapStateToProps = (state) => ({
    value: state.course,
    courseProviderId: state.courseProviderLoginId,
    courseProviderIdName: state.courseProviderLoginName
})

const RegisterCourse = connect(
    mapStateToProps,
    mapDispatchToProps
)(CourseRegistration)


export default RegisterCourse;