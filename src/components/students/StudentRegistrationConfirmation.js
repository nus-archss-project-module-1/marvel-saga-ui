import React, {Component} from 'react';
import {connect} from 'react-redux'
import {Button, TextField} from "@material-ui/core";
import {Redirect} from "react-router-dom";

import { Auth, Hub } from 'aws-amplify';

export class StudentRegistrationConfirmation extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            user: {
                username: '',
                confirmCode: '',
                moduleType: 'student',
            },
            errorMessage: '',
            confirmed: ''
        };
    }

    handleOnChange() {
        return (e) => {
            e.preventDefault();
            const {name, value} = e.target;
            const user = this.state.user;
            user[name] = value;
            this.setState({user});
        };
    }

    render() {
        const submitForm = async (event) => {
            event.preventDefault();
            this.setState({errorMessage: ''})
            console.log('this.props.studentRegistrationConfirmation:', this.props.studentRegistrationConfirmation);
            try {
                await Auth.confirmSignUp(
                    this.props.studentRegistrationConfirmation,
                    this.state.user.confirmCode
                );
            } catch (error) {
                this.setState({errorMessage: 'Invalid Confirmation Code', confirmed: false})
                console.log('error signing up:', error);
                // FIXME redirect error page
            }

            this.setState({confirmed: true});
        }

        if (this.state.confirmed) {
            return <Redirect to='/'/>
        }

        return (
            <div align="center">
                <br/>
                <span>Successfully registered: {this.props.studentRegistrationConfirmation}</span>
                {<h4 className="error"> { this.state.errorMessage } </h4> }
                <br/>
                <br/>
                <TextField
                    variant="outlined"
                    id="confirmCode"
                    name="confirmCode"
                    label="Confirmation Code"
                    fullWidth
                    value={this.state.user.confirmCode}
                    onChange={this.handleOnChange()}
                />
                <Button variant="contained" color="primary" onClick={ submitForm }>
                    Confirm Register
                </Button>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    studentRegistrationConfirmation: state.studentRegistrationConfirmation,
})

export default connect(mapStateToProps)(StudentRegistrationConfirmation);
