import React, {Component} from "react";
import {Button, Container, CssBaseline, TextField} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import StudentCoursesGrid from "./StudentCoursesGrid";
import Autocomplete from '@material-ui/lab/Autocomplete';
import {COURSE_CATEGORY, COURSE_SUB_CATEGORY} from "../../common/CourseConstants";
import {postApi} from "../../../api/ApiService";
import {connect} from "react-redux";
import urls from '../../../api/ApiEndpoints.json';

class CourseEnrollment extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            category: '',
            subCategory: '',
            ageGroup: '',
            location: '',
            searchedCourses: [],
            courseProviderName: ''
        }
    }

    render() {
        const search = async (event) => {
            event.preventDefault();

            console.log("Pradeep:", this.props.studentLoginId)
            let courseSearchParameters = {
                'studentId': this.props.studentLoginId,
                'category': this.state.category,
                'subCategory': this.state.subCategory,
                'ageGroup': this.state.ageGroup,
                'location': this.state.location
            };

            console.log("Searching Courses with: ", courseSearchParameters)
            postApi(urls[process.env.URL_CONFIG].courseProviderUrl + '/marvel-course-provider/searchCourses/', courseSearchParameters)
                .then(response => {
                    // console.log("Fetched courses: ", response.data)
                    this.setState({searchedCourses: response.data})
                })
                .catch(error => {
                    console.log("Error occurred while fetching student information: ", error.data)
                });
        }


        const resetForm = (event) => {
            event.preventDefault();
            this.setState(
                {
                    category: '',
                    subCategory: '',
                    ageGroup: '',
                    location: '',
                    searchedCourses: [],
                    courseProviderName: ''
                }
            );
        }

        return (
            <React.Fragment>
                <CssBaseline/>

                <Container maxWidth="xl">
                    <br/>
                    <Typography align="center" gutterBottom variant="h6" component="h2">
                        Course Enrollment
                    </Typography>
                    <form noValidate autoComplete="off">
                        <table align="center">
                            <tbody>
                            <tr>
                                <td>
                                    <Autocomplete
                                        id="course-category"
                                        options={COURSE_CATEGORY}
                                        getOptionLabel={(option) => option.title}
                                        onInputChange={(event, value) => {
                                            this.setState({category: value})
                                        }
                                        }
                                        // style={{width: 300}}
                                        renderInput={(params) =>
                                            <TextField {...params}
                                                       id="category"
                                                       name="category"
                                                       label="Course Category"
                                                       value={this.state.category}
                                                       variant="outlined"/>}
                                    />
                                </td>
                                <td>
                                    <Autocomplete
                                        id="sub-category"
                                        options={COURSE_SUB_CATEGORY}
                                        getOptionLabel={(option) => option.title}
                                        onInputChange={(event, value) => {
                                            this.setState({subCategory: value})
                                        }
                                        }
                                        // style={{width: 300}}
                                        renderInput={(params) =>
                                            <TextField {...params}
                                                       id="subcategory"
                                                       name="subCategory"
                                                       label="Sub Category"
                                                       value={this.state.subCategory}
                                                       variant="outlined"/>}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <TextField
                                        variant="outlined"
                                        id="location"
                                        name="location"
                                        label="Location"
                                        fullWidth
                                        value={this.state.location}
                                        onChange={(event) => {
                                            this.setState({location: event.target.value})
                                        }}
                                    />
                                </td>
                                <td>
                                    <TextField
                                        variant="outlined"
                                        id="contactNumber"
                                        name="ageGroup"
                                        label="Age group"
                                        value={this.state.ageGroup}
                                        fullWidth
                                        onChange={(event) => {
                                            this.setState({ageGroup: event.target.value})
                                        }}
                                    />
                                </td>
                            </tr>
                            </tbody>
                            <tfoot align="center">
                            <tr>
                                <td>
                                    <br/>
                                    <Button variant="contained" color="primary" onClick={search}>
                                        Search
                                    </Button>
                                </td>
                                <td>
                                    <br/>
                                    <Button variant="contained" color="primary" onClick={resetForm}>
                                        Clear
                                    </Button>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </form>
                    <br/>
                    <br/>
                    <StudentCoursesGrid type={"ENROL"} searchedCourses={this.state.searchedCourses}/>
                    <br/>
                    <br/>
                </Container>
            </React.Fragment>
        )
    }
}


const mapStateToProps = (state) => ({
    studentLoginId: state.studentLoginId,
})

export default connect(mapStateToProps,null)(CourseEnrollment);