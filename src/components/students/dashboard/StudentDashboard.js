import React, {useEffect, useState} from 'react';
import clsx from 'clsx';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import Box from '@material-ui/core/Box';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import NotificationsIcon from '@material-ui/icons/Notifications';
import {DashboardMenu, secondaryListItems} from './StudentMenuItems';
import {Redirect, withRouter} from "react-router-dom";
import Copyright from "../../common/CopyRight";
import CourseEnrollment from "./CourseEnrollment";
import StudentCoursesGrid from "./StudentCoursesGrid";
import Style from "./Style";
import StudentInsightGrid from "./StudentInsightGrid";
import { useHistory } from "react-router-dom";
import SessionTimeout from '../../common/SessionTimeout';

import { Auth, Hub } from 'aws-amplify';

const useStyles = Style();

function DashboardAppBar(open, handleDrawerOpen) {
    const classes = useStyles();

    return <AppBar position="absolute" className={clsx(classes.appBar, open && classes.appBarShift)}>
        <Toolbar className={classes.toolbar}>
            <IconButton
                edge="start"
                color="inherit"
                aria-label="open drawer"
                onClick={handleDrawerOpen}
                className={clsx(classes.menuButton, open && classes.menuButtonHidden)}
            >
                <MenuIcon/>
            </IconButton>
            <Typography align="center" component="h1" variant="h6" color="inherit" noWrap
                        className={classes.title}>
                Marvel Student Dashboard
            </Typography>
            <IconButton color="inherit">
                <Badge badgeContent={4} color="secondary">
                    <NotificationsIcon/>
                </Badge>
            </IconButton>
        </Toolbar>
    </AppBar>;
}

export function StudentDashboard() {
    let history = useHistory();
    const classes = useStyles();
    const [open, setOpen] = useState(true);
    const [showNewCourse, setShowNewCourse] = useState(false);
    const [showReports, setShowReports] = useState(false);
    const [showCourses, setShowCourses] = useState(false);
    const [showMyCourses, setShowMyCourses] = useState(false);
    const [showDashboard, setShowDashboard] = useState(true);

    const [logout, setLogout] = useState(false);
    const [isStudentAuthenticated, setStudentAuthentication] = useState(false);
    // const [sessionTimeout, setSessionTimeout] = useState(0);
    // const [timeout, setTimeout] = useState(0);

    const resetDisplay = () => {
        setShowReports(false);
        setShowNewCourse(false);
        setShowCourses(false);
        setShowMyCourses(false);
        setShowDashboard(false);
    };

    const handleNewCourse = () => {
        resetDisplay();
        setShowNewCourse(true);
    };
    const handleDashboard = () => {
        resetDisplay();
        setShowReports(true);
        setShowNewCourse(false);
        setShowCourses(true);
        setShowMyCourses(false);
    };

    const handleMyCourses = () => {
        resetDisplay();
        setShowMyCourses(true);
    };

    const handleLogout = async () => {
        console.log('log out click');
        try {
            // Sign out user
            await Auth.signOut();
        } catch(err) {
            console.log('no user found', err)
        }
        resetDisplay();
        setLogout(true);
    };

    const handleShowReports = () => {
        resetDisplay();
        setShowReports(true);
    };

    const handleDrawerOpen = () => {
        setOpen(true);
    };
    const handleDrawerClose = () => {
        setOpen(false);
    };

    const updateTimeout = async() => {
        const session = await Auth.currentSession();
        const issuedAt = session.getAccessToken().getIssuedAt()
        const expiration = session.getAccessToken().getExpiration()
        let timeout = expiration - issuedAt

        return timeout
    };

    useEffect(async () => {
        console.log("Student Logging out.... ", logout)

        try {
            // Check existing user after logout
            const studentUser = await Auth.currentAuthenticatedUser()
            setStudentAuthentication(true);
            console.log('user is : ', studentUser)

            // const authOption = await Auth.configure()
            // console.log('authOption is : ', authOption)

        } catch(err) {
            setStudentAuthentication(false);
            setLogout(true);

            //history.push('/');
            console.log('no user found from studentdashboard', err)
        }

        if (logout) {
            console.log('logout true')
            setLogout(true);
            history.push('/');
            // history.push('/logout');
        }
        setLogout(false);
    }, [logout])

    return (
        <div className={classes.root}>
            <SessionTimeout isAuthenticated={isStudentAuthenticated} logOut={handleLogout} />
            <CssBaseline/>
            {DashboardAppBar(open, handleDrawerOpen)}
            <Drawer
                variant="permanent"
                classes={{
                    paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
                }}
                open={open}
            >
                <div className={classes.toolbarIcon}>
                    <IconButton onClick={handleDrawerClose}>
                        <ChevronLeftIcon/>
                    </IconButton>
                </div>
                <Divider/>
                <List>{DashboardMenu(handleDashboard, handleNewCourse, handleMyCourses, handleShowReports, handleLogout)}</List>
            </Drawer>
            <main className={classes.content}>
                <div className={classes.appBarSpacer}/>
                <Container maxWidth="xl" className={classes.container}>

                    {/* Dashboard*/}
                    {showDashboard ?
                        <Grid item xs={12}>
                            <Paper className={classes.paper}>
                                <CourseEnrollment/>
                            </Paper>
                        </Grid>
                        : ''}

                    {/* New Course */}
                    <Grid container spacing={3}>
                        {showNewCourse ?
                            <Grid item xs={12}>
                                <CourseEnrollment/>
                            </Grid>
                            : ''}

                        {/* Reports */}
                        {showReports ?
                            <Grid item xs={12}>
                                <Paper>
                                    <br/>
                                    <StudentInsightGrid type={"INSIGHT"}/>
                                </Paper>
                            </Grid> : ''}

                        {/* Students */}
                        {showMyCourses ?
                            <Grid item xs={12}>
                                <StudentCoursesGrid type={"MY_COURSES"}/>
                            </Grid> : ''}

                        {/* Courses */}
                        {showCourses ?
                            <Grid item xs={12}>
                                <Paper className={classes.paper}>
                                    <StudentCoursesGrid type={"MY_COURSES"}/>
                                </Paper>
                            </Grid>
                            : ''}
                    </Grid>
                    <Box pt={4}>
                        <Copyright/>
                    </Box>
                </Container>
            </main>
        </div>
    );
}

export default withRouter(StudentDashboard)