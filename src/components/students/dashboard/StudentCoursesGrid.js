import * as React from 'react';
import {useEffect, useState} from 'react';
import {DataGrid} from '@material-ui/data-grid';
import {getApi, postApi} from "../../../api/ApiService";
import Typography from "@material-ui/core/Typography";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import {Button} from "@material-ui/core";
import {useSelector} from "react-redux";
import {Redirect} from "react-router-dom";
import urls from '../../../api/ApiEndpoints.json'

const columns = [
    {field: 'id', headerName: 'ID', width: 70},
    {field: 'name', headerName: 'Course Provider Name', width: 200},
    // {field: 'courseProviderName', headerName: 'Course Provider Name', width: 200},
    {field: 'category', headerName: 'Category', width: 130},
    {field: 'subCategory', headerName: 'Sub Category', width: 160},
    {field: 'ageGroup', headerName: 'Age group', width: 130},
    {field: 'level', headerName: 'Skill Level', width: 130},
    {field: 'location', headerName: 'Location', width: 130},
    {field: 'teacher', headerName: 'Teacher', width: 130},
    {field: 'duration', headerName: 'Duration hours', width: 125},
    {field: 'classSize', headerName: 'Class size', width: 125},
    {field: 'fee', headerName: 'Fees', type: 'number', width: 90},
    {field: 'slots', headerName: 'Time Slots', width: 90},
    {field: 'frequency', headerName: 'Frequency', width: 125},
];

const EnrollmentAction = (open, selectedCourse, handleClose, handleOnBackdropClick, studentLoginId, setIsEnrolled, studentInformation) => {
    console.log("Selected course: ", selectedCourse)
    console.log("Selected Student Information: ", studentInformation)
    let result = {
        'courseId': selectedCourse.id,
        'studentId': studentInformation.id,
        'studentName': studentInformation.studentName,
        'parentName': studentInformation.parentName,
        'contactNumber': studentInformation.contactNumber,
        'gender': studentInformation.gender,
        'email': studentInformation.email
    };

    const enrolCourse = async (event) => {
        event.preventDefault();

        postApi(urls[process.env.URL_CONFIG].courseProviderUrl + '/marvel-course-provider/enrolCourse', result)
            .then(response => {
                console.log("Successfully enrolled courses", response.data)
                setIsEnrolled(true);
            })
            .catch(error => {
                console.log("Error occurred while enrolling courses: ", error.data)
                setIsEnrolled(false);
                alert('Student has already enrolled to this course: ' + selectedCourse.name)
            });
    }


    return <Dialog open={open} onClose={handleClose}
                   aria-labelledby="form-dialog-title"
                   maxWidth="lg"
                   onBackdropClick={handleOnBackdropClick}
    >
        <DialogTitle id="form-dialog-title">Course Enrollment Form</DialogTitle>
        <DialogContent>
            <DialogContentText>
                Fill your course schedule details
            </DialogContentText>
            <table align="center">
                <tbody>
                <tr>
                    <td>
                        <TextField
                            variant="outlined"
                            id="enrol-name"
                            name="name"
                            label="Course Name"
                            value={selectedCourse.name}
                            readOnly
                        />
                    </td>
                    <td>
                        <TextField
                            variant="outlined"
                            id="enrol-courseProviderName"
                            name="courseProviderName"
                            label="Course Provider Name"
                            value={selectedCourse.courseProviderName}
                            readOnly
                        />
                    </td>
                </tr>
                <tr>
                    <td>
                        <br/>
                        <TextField
                            variant="outlined"
                            id="enrol-category"
                            name="category"
                            label="Course Category"
                            value={selectedCourse.category}
                            readOnly
                        />
                    </td>
                    <td>
                        <br/>
                        <TextField
                            variant="outlined"
                            id="enrol-subCategory"
                            name="subCategory"
                            label="Sub Category"
                            fullWidth
                            value={selectedCourse.subCategory}
                            readOnly
                        />
                    </td>
                </tr>
                <tr>
                    <td>
                        <br/>
                        <TextField
                            variant="outlined"
                            id="classSize"
                            name="classSize"
                            label="Class Size"
                            value={selectedCourse.classSize}
                            fullWidth
                            readOnly
                        />
                    </td>
                    <td>
                        <br/>
                        <TextField
                            id="enrol-ageGroup"
                            name="ageGroup"
                            label="ageGroup"
                            multiline
                            fullWidth
                            variant="outlined"
                            value={selectedCourse.ageGroup}
                            readOnly
                        />
                    </td>
                </tr>
                <tr>
                    <td>
                        <br/>
                        <TextField
                            variant="outlined"
                            id="grades"
                            name="grades"
                            label="Grades"
                            fullWidth
                            value={selectedCourse.grades}
                            readOnly
                        />
                    </td>
                    <td>
                        <br/>
                        <TextField
                            variant="outlined"
                            id="level"
                            name="level"
                            label="Skill Level"
                            fullWidth
                            value={selectedCourse.level}
                            readOnly
                        />
                    </td>
                </tr>
                <tr>
                    <td>
                        <br/>
                        <TextField
                            variant="outlined"
                            id="teacher"
                            name="teacher"
                            label="Teacher"
                            fullWidth
                            value={selectedCourse.teacher}
                            readOnly
                        />
                    </td>
                    <td>
                        <br/>
                        <TextField
                            variant="outlined"
                            id="frequency"
                            name="frequency"
                            label="Frequency"
                            fullWidth
                            value={selectedCourse.frequency}
                            readOnly
                        />
                    </td>
                </tr>
                <tr>
                    <td>
                        <br/>
                        <TextField
                            variant="outlined"
                            id="duration"
                            name="duration"
                            label="Duration"
                            fullWidth
                            value={selectedCourse.duration}
                            readOnly
                        />
                    </td>
                    <td>
                        <br/>
                        <TextField
                            variant="outlined"
                            id="fee"
                            name="fee"
                            label="Fee"
                            fullWidth
                            value={selectedCourse.fee}
                            readOnly
                        />
                    </td>
                    <td/>
                </tr>
                <tr>
                    <td>
                        <br/>
                        <TextField
                            variant="outlined"
                            id="location"
                            name="location"
                            label="Location"
                            fullWidth
                            value={selectedCourse.location}
                            readOnly
                        />
                    </td>
                    <td/>
                </tr>
                </tbody>
            </table>
            <div align={"center"}>
                <br/>
                <Button variant="contained" color="primary" onClick={enrolCourse}>
                    Enrol
                </Button>
            </div>
        </DialogContent>
    </Dialog>;
}

export default function StudentCoursesGrid(props) {
    const [courseList, setCourseList] = useState([]);
    const [selectedCourse, setSelectedCourse] = useState({});
    const [action, setAction] = useState(false);
    const [rowSelected, setRowSelected] = useState(false);
    const [open, setOpen] = React.useState(true);

    const [isEnrolled, setIsEnrolled] = React.useState(false);
    const [studentInformation, setStudentInformation] = useState();
    const studentLoginId = useSelector(state => state.studentLoginId)

    const handleClose = () => {
        setOpen(false);
    };

    const handleOnBackdropClick = () => {
        console.log("onBackdropClick")
        setAction(false)
        setRowSelected(false)
    }

    useEffect(() => {
        if (props.type === "MY_COURSES") {
            console.log("Fetching my courses for Student id: ", studentLoginId)
            getApi(urls[process.env.URL_CONFIG].courseProviderUrl + '/marvel-course-provider/fetchMyCourses/' + studentLoginId)
                .then(response => {
                    setCourseList(response.data)
                })
                .catch(error => {
                    console.log("Error occurred while fetching student courses: ", error.data)
                });
        } else {
            console.log("Searched courses result: ", props.searchedCourses)
            if (props.searchedCourses.length === 0) {
                console.log("Fetching all courses for enrollment")
                getApi(urls[process.env.URL_CONFIG].courseProviderUrl + '/marvel-course-provider/fetchCourses')
                    .then(response => {
                        setCourseList(response.data)
                    })
                    .catch(error => {
                        console.log("Error occurred while fetching courses: ", error.data)
                    });
            } else {
                setCourseList(props.searchedCourses)
            }
        }

        console.log("Fetching Student information: ", studentLoginId)
        getApi(urls[process.env.URL_CONFIG].studentsUrl + '/marvel-student/fetchStudent/' + studentLoginId)
            .then(response => {
                setStudentInformation(response.data)
            })
            .catch(error => {
                console.log("Error occurred while fetching student information: ", error.data)
            });

    }, [props.searchedCourses])

    useEffect(() => {
        console.log("useEffect ", action)
        if (rowSelected) {
            setAction(true)
        }
    }, [selectedCourse])

    const handleRowSelected = (params) => {
        console.log(params)
        if (params !== "undefined") {
            setSelectedCourse(params.row)
            setRowSelected(true);
            setAction(true);
            setOpen(true);
            console.log("handleRowSelected ", action)
        }
    }

    if (isEnrolled) {
        console.log("Student successfully enrolled: ", isEnrolled)
        return <Redirect to='/home'/>;
    }

    return (
        <div style={{height: 400, width: '100%'}}>

            <Typography align="center" gutterBottom variant="h6" component="h2">
                {(props.type === "MY_COURSES") ? 'My Enrolled Courses' : 'Available Courses'}
            </Typography>

            {action === true && EnrollmentAction(open, selectedCourse, handleClose, handleOnBackdropClick, studentLoginId, setIsEnrolled, studentInformation)}
            <DataGrid rows={courseList} columns={columns}
                      density="standard"
                      pageSize={5}
                      disableMultipleSelection={true}
                      onRowDoubleClick={handleRowSelected}
            />

            <Typography gutterBottom align='right' color="primary" variant="button">
                Double click the row to enroll.
            </Typography>
        </div>
    )
        ;
}
