import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import DashboardIcon from '@material-ui/icons/Dashboard';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import StreetviewIcon from '@material-ui/icons/Streetview';
import BarChartIcon from '@material-ui/icons/BarChart';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import Divider from "@material-ui/core/Divider";

export function DashboardMenu(handleDashboard, handleNewCourse, handleMyCourses, handleShowReports, handleLogout) {
    return <div>
        <ListItem button>
            <ListItemIcon>
                <DashboardIcon/>
            </ListItemIcon>
            <ListItemText primary="Dashboard" onClick={handleDashboard}/>
        </ListItem>
        <ListItem button>
            <ListItemIcon>
                <ShoppingCartIcon/>
            </ListItemIcon>
            <ListItemText primary="Enrol Course" onClick={handleNewCourse}/>
        </ListItem>
        <ListItem button>
            <ListItemIcon>
                <StreetviewIcon/>
            </ListItemIcon>
            <ListItemText primary="My Courses" onClick={handleMyCourses}/>
        </ListItem>
        <ListItem button>
            <ListItemIcon>
                <BarChartIcon/>
            </ListItemIcon>
            <ListItemText primary="Insight 360" onClick={handleShowReports}/>
        </ListItem>
        <Divider/>
        <ListItem button>
            <ListItemIcon>
                <ExitToAppIcon/>
            </ListItemIcon>
            <ListItemText primary="Log out" onClick={handleLogout}/>
        </ListItem>
    </div>;
}

export const secondaryListItems = (
    <div>
        <ListItem button>
            <ListItemIcon>
                <ExitToAppIcon />
            </ListItemIcon>
            <ListItemText primary="Log out"/>
        </ListItem>
    </div>
);