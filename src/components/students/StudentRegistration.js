import React, {Component} from 'react';
import {
    Button,
    Checkbox,
    Container,
    CssBaseline,
    FormControlLabel,
    FormGroup,
    MenuItem,
    Paper,
    TextField
} from "@material-ui/core";
import {genderList} from "../constants/ScreenConstants"
import {studentRegistered} from "../../actions";
import {connect} from "react-redux";
import {REGISTER_STUDENT} from "../../actions/ActionConstants";
import Typography from "@material-ui/core/Typography";
import {Redirect} from "react-router-dom";
import Grid from "@material-ui/core/Grid";

import {Auth} from 'aws-amplify';
import {loadConsentData} from "./LoadConsentData";

class StudentRegistration extends Component {

    constructor(props) {
        super(props);
        this.state = this.initialize()
    }

    initialize() {
        return {
            user: {
                studentName: '',
                studentNric: '',
                parentName: '',
                parentNric: '',
                contactNumber: '',
                gender: 'Male',
                dob: '',
                email: '',
                district: '',
                country: '',
                address: '',
                postalCode: '',
                userPassword: '',
                confirmPassword: '',
                interests: 'none',
                loginType: 'custom',
                moduleType: 'student',
                token: '',
            },
            errors: {
                user: {
                    studentName: 'Enter Student Name',
                    parentName: 'Enter Parent Name',
                    contactNumber: 'Enter Contact Number',
                    email: 'Email is not valid',
                    address: 'Enter address',
                    district: 'Enter District',
                    country: 'Enter Country',
                    dob: 'Enter DOB',
                }
            },
            validForm: false,
            submitted: false,
            cancelled: false,
            isLoading: false,
            hasErrored: false,
            disableSubmit: true

        };
    }

    async componentDidMount() {

        try {
            // Check existing user after logout
            const userInfo = await Auth.currentUserInfo()

            const user = this.state.user;
            user.studentName = userInfo.attributes.name;
            user.email = userInfo.attributes.email;

            const username = userInfo.username;
            user.loginType = username.substring(0, username.indexOf("_"));

            const session = await Auth.currentSession();
            user.token = session.getAccessToken().getJwtToken();

            console.log('user state', user);

            this.setState({user});

        } catch (err) {
            console.log('no user found from studentregistration', err)
        }

    }

    handleOnChange() {
        return (e) => {
            e.preventDefault();
            const {name, value} = e.target;
            const user = this.state.user;
            user[name] = value;
            this.setState({user});
        };
    }


    render() {

        const submitForm = async (event) => {
            event.preventDefault();
            console.log(this.state.user);
            console.log('this.state.user.loginType:', this.state.user.loginType);
            await this.props.registerStudent(this.state);

            if (this.state.user.loginType === "custom") {
                try {
                    await Auth.signUp({
                        username: this.state.user.email,
                        password: this.state.user.userPassword,
                        attributes: {email: this.state.user.email}
                    });
                } catch (error) {
                    console.log('error signing up:', error);
                }
            }

            this.setState({submitted: true});
        }

        const cancelForm = async (event) => {
            event.preventDefault();
            try {
                // Sign out user
                await Auth.signOut();
            } catch (err) {
                console.log('no user found', err)
            }
            this.setState({cancelled: true});
            this.setState({submitted: true});
        }

        const onConsent = async (event) => {
            event.preventDefault();
            if(event.target.checked){
                this.setState({disableSubmit: false});
            }else{
                this.setState({disableSubmit: true});
            }
        }

        const handleInterest = (event) => {
            const user = this.state.user;
            user['interests'] = user.interests + ',' + event.target.name;
            this.setState({user});
        }

        if (this.state.submitted && this.state.cancelled) {
            return <Redirect to='/'/>
        }

        if (this.state.submitted && this.state.user.loginType === "custom") {
            return <Redirect to='/confirmStudent'/>
        }

        if (this.state.submitted && this.state.user.loginType !== "custom") {
            return <Redirect to='/studentDashboard'/>
        }

        const resetForm = (event) => {
            event.preventDefault();
            this.setState({
                user: {
                    studentName: '',
                    studentNric: '',
                    parentName: '',
                    parentNric: '',
                    contactNumber: '',
                    gender: '',
                    dob: '',
                    email: '',
                    district: '',
                    country: '',
                    address: '',
                    postalCode: '',
                    studentPassword: '',
                    confirmStudentPassword: '',
                    loginType: 'custom',
                    moduleType: 'student',
                    token: '',
                }
            });
            console.log(this.state.user)
        }

        return (
            <React.Fragment>
                <CssBaseline/>

                <Container maxWidth="md">
                    <Paper>
                        <br/>
                        <Typography align="center" gutterBottom variant="h6" component="h2">
                            Student Registration Form
                        </Typography>
                        <form noValidate autoComplete="off">
                            <Grid container spacing={3}>
                                <Grid item xs={12} sm={1}/>
                                <Grid item xs={12} sm={6}>
                                    <table align="center">
                                        <tbody>
                                        <tr>
                                            <td>
                                                <br/>
                                                <TextField
                                                    variant="outlined"
                                                    id="studentName"
                                                    name="studentName"
                                                    label="Student Name"
                                                    fullWidth
                                                    value={this.state.user.studentName}
                                                    onChange={this.handleOnChange()}
                                                />
                                            </td>
                                            <td>
                                                <br/>
                                                <TextField
                                                    variant="outlined"
                                                    id="studentNric"
                                                    name="studentNric"
                                                    label="NRIC"
                                                    fullWidth
                                                    value={this.state.user.studentNric}
                                                    onChange={this.handleOnChange()}
                                                />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <br/>
                                                <TextField
                                                    variant="outlined"
                                                    id="dob"
                                                    name="dob"
                                                    label="Birthday"
                                                    type="date"
                                                    fullWidth
                                                    InputLabelProps={{
                                                        shrink: true,
                                                    }}
                                                    value={this.state.user.dob}
                                                    onChange={this.handleOnChange()}
                                                />
                                            </td>
                                            <td>
                                                <br/>
                                                <TextField
                                                    variant="outlined"
                                                    id="gender"
                                                    name="gender"
                                                    label="Gender"
                                                    fullWidth
                                                    value={this.state.user.gender}
                                                    onChange={this.handleOnChange()}
                                                >
                                                    {genderList.map((option) => (
                                                        <MenuItem key={option.value} value={option.value}>
                                                            {option.label}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <br/>
                                                <TextField
                                                    variant="outlined"
                                                    id="parentName"
                                                    name="parentName"
                                                    label="Parent Name"
                                                    fullWidth
                                                    value={this.state.user.parentName}
                                                    onChange={this.handleOnChange()}
                                                />
                                            </td>
                                            <td>
                                                <br/>
                                                <TextField
                                                    variant="outlined"
                                                    id="parentNric"
                                                    name="parentNric"
                                                    label="NRIC"
                                                    fullWidth
                                                    value={this.state.user.parentNric}
                                                    onChange={this.handleOnChange()}
                                                />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <br/>
                                                <TextField
                                                    variant="outlined"
                                                    id="email"
                                                    name="email"
                                                    label="Email"
                                                    fullWidth
                                                    value={this.state.user.email}
                                                    onChange={this.handleOnChange()}
                                                />
                                            </td>
                                            <td>
                                                <br/>
                                                <TextField
                                                    variant="outlined"
                                                    id="contactNumber"
                                                    name="contactNumber"
                                                    label="Contact Number"
                                                    value={this.state.user.contactNumber}
                                                    fullWidth
                                                    onChange={this.handleOnChange()}
                                                />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <br/>
                                                <TextField
                                                    id="address"
                                                    name="address"
                                                    label="Address"
                                                    multiline
                                                    fullWidth
                                                    variant="outlined"
                                                    value={this.state.user.address}
                                                    onChange={this.handleOnChange()}
                                                />
                                            </td>
                                            <td>
                                                <br/>
                                                <TextField
                                                    variant="outlined"
                                                    id="district"
                                                    name="district"
                                                    label="District"
                                                    fullWidth
                                                    value={this.state.user.district}
                                                    onChange={this.handleOnChange()}
                                                />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <br/>
                                                <TextField
                                                    variant="outlined"
                                                    id="country"
                                                    name="country"
                                                    label="Country"
                                                    fullWidth
                                                    value={this.state.user.country}
                                                    onChange={this.handleOnChange()}
                                                />
                                            </td>
                                            <td>
                                                <br/>
                                                <TextField
                                                    variant="outlined"
                                                    id="postalCode"
                                                    name="postalCode"
                                                    label="Postal Code"
                                                    fullWidth
                                                    value={this.state.user.postalCode}
                                                    onChange={this.handleOnChange()}
                                                />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <br/>
                                                <TextField
                                                    variant="outlined"
                                                    id="students-userPassword"
                                                    type="password"
                                                    name="userPassword"
                                                    label="Password"
                                                    fullWidth autoComplete="off"
                                                    value={this.state.user.userPassword}
                                                    onChange={this.handleOnChange()}
                                                />
                                            </td>
                                            <td>
                                                <br/>
                                                <TextField
                                                    variant="outlined"
                                                    type="password"
                                                    id="students-confirmUserPassword"
                                                    name="confirmPassword"
                                                    label="Confirm Password"
                                                    fullWidth autoComplete="off"
                                                    value={this.state.user.confirmPassword}
                                                    onChange={this.handleOnChange()}
                                                />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>

                                            </td>
                                            <td>

                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </Grid>
                                <Grid item xs={12} sm={5}>
                                    <br/>
                                    <Typography align="left" gutterBottom variant="subtitle1">
                                        Select your interest category
                                    </Typography>
                                    <FormGroup row>
                                        <FormControlLabel
                                            control={
                                                <Checkbox name="Science" color="primary" onChange={handleInterest}
                                                />
                                            }
                                            label="Science"
                                        />
                                        <FormControlLabel
                                            control={<Checkbox name="Reading" color="primary"
                                                               onChange={handleInterest}/>}
                                            label="Reading"
                                        />
                                    </FormGroup>
                                    <FormGroup row>
                                        <FormControlLabel
                                            control={
                                                <Checkbox
                                                    name="Maths"
                                                    color="primary" onChange={handleInterest}
                                                />
                                            }
                                            label="Maths"
                                        />
                                    </FormGroup>
                                    <FormGroup row>
                                        <FormControlLabel
                                            control={
                                                <Checkbox
                                                    name="Table Tennis"
                                                    color="primary" onChange={handleInterest}
                                                />
                                            }
                                            label="Table Tennis"
                                        />
                                        <FormControlLabel
                                            control={
                                                <Checkbox
                                                    name="Tennis"
                                                    color="primary" onChange={handleInterest}
                                                />
                                            }
                                            label="Tennis"
                                        />
                                    </FormGroup>
                                    <FormGroup row>
                                        <FormControlLabel
                                            control={
                                                <Checkbox
                                                    name="Chess"
                                                    color="primary" onChange={handleInterest}
                                                />
                                            }
                                            label="Chess"
                                        />
                                        <FormControlLabel
                                            control={
                                                <Checkbox
                                                    name="Soccer"
                                                    color="primary" onChange={handleInterest}
                                                />
                                            }
                                            label="Soccer"
                                        />
                                    </FormGroup>
                                    <FormGroup row>
                                        <FormControlLabel
                                            control={
                                                <Checkbox
                                                    name="Swimming"
                                                    color="primary" onChange={handleInterest}
                                                />
                                            }
                                            label="Swimming"
                                        />
                                        <FormControlLabel
                                            control={
                                                <Checkbox
                                                    name="Badminton"
                                                    color="primary" onChange={handleInterest}
                                                />
                                            }
                                            label="Badminton"
                                        />

                                    </FormGroup>
                                    <FormGroup row>
                                        <FormControlLabel
                                            control={
                                                <Checkbox
                                                    name="Culinary Arts"
                                                    color="primary" onChange={handleInterest}
                                                />
                                            }
                                            label="Culinary Arts"
                                        />
                                        <FormControlLabel
                                            control={
                                                <Checkbox
                                                    name="Visual Arts"
                                                    color="primary" onChange={handleInterest}
                                                />
                                            }
                                            label="Visual Arts"
                                        />
                                    </FormGroup>
                                    <FormGroup row>
                                        <FormControlLabel
                                            control={
                                                <Checkbox
                                                    name="Speech and Drama"
                                                    color="primary" onChange={handleInterest}
                                                />
                                            }
                                            label="Speech and Drama"
                                        />

                                    </FormGroup>
                                    <FormGroup row>
                                        <FormControlLabel
                                            control={
                                                <Checkbox
                                                    name="English"
                                                    color="primary" onChange={handleInterest}
                                                />
                                            }
                                            label="English Language"
                                        />
                                        <FormControlLabel
                                            control={
                                                <Checkbox
                                                    name="Tamil"
                                                    color="primary" onChange={handleInterest}
                                                />
                                            }
                                            label="Tamil Language"
                                        />
                                        <FormControlLabel
                                            control={
                                                <Checkbox
                                                    name="German"
                                                    color="primary" onChange={handleInterest}
                                                />
                                            }
                                            label="German Language"
                                        />
                                        <FormControlLabel
                                            control={
                                                <Checkbox
                                                    name="French"
                                                    color="primary" onChange={handleInterest}
                                                />
                                            }
                                            label="French Language"
                                        />
                                    </FormGroup>
                                    <FormGroup row>
                                        <FormControlLabel
                                            control={
                                                <Checkbox
                                                    name="Mandarin"
                                                    color="primary" onChange={handleInterest}
                                                />
                                            }
                                            label="Mandarin Language"
                                        />
                                        <FormControlLabel
                                            control={
                                                <Checkbox
                                                    name="Hindi"
                                                    color="primary" onChange={handleInterest}
                                                />
                                            }
                                            label="Hindi Language"
                                        />
                                    </FormGroup>
                                </Grid>
                                <Grid item xs={12} sm={5}>
                                    <table align="center">
                                        <tbody>
                                        <tr>
                                            <td>
                                                <br/>
                                                {loadConsentData()}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <br/>
                                                <center><b>ACKNOWLEDGEMENT AND CONSENT</b></center>
                                                <br/><i>
                                                <FormGroup>
                                                    <FormControlLabel
                                                        control={
                                                            <Checkbox onChange={onConsent} name="consent" />
                                                        }
                                                        label="I acknowledge that I have read and understood the above Data Protection Notice, and consent to
                                                the collection, use and disclosure of my personal data by www.marvelhub.org for the purposes set
                                                out in the Notice."
                                                    />
                                                </FormGroup>
                                                </i>
                                            </td>
                                        </tr>
                                        </tbody>
                                        <tfoot align="center">
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <br/>
                                                            <Button variant="contained" color="primary" name="submitButton" id="submitButton"
                                                                    disabled={this.state.disableSubmit} onClick={submitForm}>
                                                                Submit
                                                            </Button>
                                                        </td>
                                                        <td>
                                                            <br/>
                                                            <Button variant="contained" color="primary"
                                                                    onClick={resetForm}>
                                                                Clear
                                                            </Button>
                                                        </td>
                                                        <td>
                                                            <br/>
                                                            <Button variant="contained" color="primary"
                                                                    onClick={cancelForm}>
                                                                Cancel
                                                            </Button>
                                                        </td>
                                                    </tr>
                                                </table>

                                            </td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </Grid>
                            </Grid>
                        </form>
                        <br/>
                    </Paper>
                </Container>
            </React.Fragment>

        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        isRegistered: studentRegistered,
        registerStudent: (value) =>
            dispatch({
                type: REGISTER_STUDENT,
                payload: value
            })
    };
};

const mapStateToProps = (state) => ({
    value: state.user,
})

const RegisterStudent = connect(
    mapStateToProps,
    mapDispatchToProps
)(StudentRegistration)


export default RegisterStudent;
