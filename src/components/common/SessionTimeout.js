import React, { useRef, useState, useEffect } from "react";
import IdleTimer from "react-idle-timer";
import SessionTimeoutDialog from "./SessionTimeoutDialog";

import { Auth, Hub } from 'aws-amplify';

let countdownInterval;
let timeout;
let diff;
// let userSessionTimeout;

const SessionTimeout = ({isAuthenticated, logOut}) => {
    const [timeoutModalOpen, setTimeoutModalOpen] = useState(false);
    const [timeoutCountdown, setTimeoutCountdown] = useState(0);
    const [timeoutUserSession, setTimeoutUserSession] = useState(0);

    useEffect(async () => {
        const session = await Auth.currentSession();
        const issuedAt = session.getAccessToken().getIssuedAt()
        const expiration = session.getAccessToken().getExpiration()
        // diff = (expiration-issuedAt)*1000
        diff = 60000

        console.log('timeout is : ', diff);

    },[]);

    const idleTimer = useRef(null);
    const clearSessionTimeout = () => {
        clearTimeout(timeout);
    };
    const clearSessionInterval = () => {
        clearInterval(countdownInterval);
    };
    const handleLogout = async (isTimedOut = false) => {
        try {
            setTimeoutModalOpen(false);
            clearSessionInterval();
            clearSessionTimeout();
            logOut();
        } catch (err) {
            console.error(err);
        }
    };
    const handleContinue = () => {
        setTimeoutModalOpen(false);
        clearSessionInterval();
        clearSessionTimeout();
    };
    const onActive = () => {
        if (!timeoutModalOpen) {
            clearSessionInterval();
            clearSessionTimeout();
        }
    };
    const onIdle = () => {
        const delay = 1000 * 1;
        if (isAuthenticated && !timeoutModalOpen) {
            timeout = setTimeout(() => {
                let countDown = 10;
                setTimeoutModalOpen(true);
                setTimeoutCountdown(countDown);
                countdownInterval = setInterval(() => {
                    if (countDown > 0) {
                        setTimeoutCountdown(--countDown);
                    } else {
                        handleLogout(true);
                    }
                }, 1000);
            }, delay);
        }
    };
    return (
        <div>
            <IdleTimer
                ref={idleTimer}
                onActive={onActive}
                onIdle={onIdle}
                debounce={250}
                timeout={diff}
            />
            <SessionTimeoutDialog
                countdown={timeoutCountdown}
                onContinue={handleContinue}
                onLogout={() => handleLogout(false)}
                open={timeoutModalOpen}
            />
        </div>
    );
}
export default SessionTimeout;