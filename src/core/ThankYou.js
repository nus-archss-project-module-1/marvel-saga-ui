import React from 'react';
import Typography from "@material-ui/core/Typography";


class ThankYou extends React.Component {
    render() {
        return (
            <div align="center">
                <br/>
                <Typography gutterBottom variant="h6" component="h2">
                    Thank you for using Marvel enrichment hub
                </Typography>
            </div>
        );
    }
}


export default ThankYou;
