import React, {Component} from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import RegisterStudent from "../components/students/StudentRegistration";
import StudentRegistrationConfirmation from "../components/students/StudentRegistrationConfirmation";
import RegisterCourseProvider from "../components/courseProviders/CourseProviderRegistration";
import RegisterCourse from "../components/courses/CourseRegistration";
import CourseProviderConfirmation from "../components/courseProviders/CourseProviderConfirmation";
import CourseConfirmation from "../components/courses/CourseConfirmation";
import LandingPage from "./LandingPage";
import CourseProviderDashboard from "../components/courseProviders/dashboard/CourseProviderDashboard";
import {StudentDashboard} from "../components/students/dashboard/StudentDashboard";
import ThankYou from "./ThankYou";

class Navigation extends Component {
    render() {
        return (
            <Router>
                <Switch>
                    <Route path="/home"><LandingPage/></Route>
                    <Route path="/logout"><ThankYou/></Route>
                    <Route path="/registerStudent"><RegisterStudent/></Route>
                    <Route path="/confirmStudent"><StudentRegistrationConfirmation/></Route>
                    <Route path="/registerCourseProvider"><RegisterCourseProvider/></Route>
                    <Route path="/confirmCourseProvider"><CourseProviderConfirmation/></Route>
                    <Route path="/courseProviderDashboard"><CourseProviderDashboard/></Route>
                    <Route path="/studentDashboard"><StudentDashboard/></Route>
                    <Route path="/registerCourse"><RegisterCourse/></Route>
                    <Route path="/confirmCourse"><CourseConfirmation/></Route>
                    <Route exact path="/">
                        <LandingPage/>
                    </Route>
                </Switch>
            </Router>
        )
    }
}

export default Navigation;