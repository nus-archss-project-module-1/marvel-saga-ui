export default function PageNotFound() {
    return (
        <div align="center">
            <h1>Page not found!</h1>
        </div>
    )
}

import React from 'react';