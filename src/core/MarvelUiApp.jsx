import React from 'react';
import Loading from '../components/common/Loading'
import Navigation from "./Navigation";
import HeaderBar from "./HeaderBar";


let MarvelStudentApp = () => (
    <div>
        <HeaderBar/>
        <Loading/>
        <Navigation/>
    </div>
);


export default MarvelStudentApp;
