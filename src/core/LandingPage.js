import React from 'react';
import StudentsCard from "../components/login/StudentsCard";
import CourseProvidersCard from "../components/login/CourseProvidersCard";


class LandingPage extends React.Component {
    render() {
        return (
            <div align="center">
                <br/>
                <br/>
                <br/>
                <br/>
                <table>
                    <tbody>
                    <tr>
                        <td>
                            <StudentsCard/>
                        </td>
                        <td/>
                        <td/>
                        <td>
                            <CourseProvidersCard/>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        );
    }
}


export default LandingPage;
