// Worker
import {call, put, takeEvery} from "redux-saga/effects";
import {postApi} from "../api/ApiService";
import {
    COURSE_PROVIDER_REGISTRATION_STATUS,
    REGISTER_COURSE_PROVIDER
} from "../actions/ActionConstants";
import urls from "../api/ApiEndpoints.json";

// Worker
function* courseProviderRegistered(action) {
    try {
        console.log("invoking endpoint", action.payload.courseProvider)
        let URL = urls[process.env.URL_CONFIG].courseProviderUrl + '/marvel-course-provider/register';
        console.log("Course provider registration URL: ", URL)

        const registeredCourseProvider = yield call(postApi, URL, action.payload.courseProvider);
        console.log("Course Provider Registered ->>>:", registeredCourseProvider.data);
        yield put({
            type: COURSE_PROVIDER_REGISTRATION_STATUS,
            json: registeredCourseProvider.data.name || [{error: registeredCourseProvider.data.message}],
            // json: registeredCourseProvider.data.message || [{error: registeredCourseProvider.data.message}],
            isRegistered: true
        });
    } catch (err) {
        console.log(err.response.data.message)
        yield put({type: COURSE_PROVIDER_REGISTRATION_STATUS, json: err, isRegistered: false});
        // yield put({type: COURSE_PROVIDER_REGISTRATION_STATUS, json: err.response.data.message, isRegistered: false});
    }
}

// Watcher
export function* registerCourseProviderSaga() {
    console.log("Saga registerStudent")
    yield takeEvery(REGISTER_COURSE_PROVIDER, courseProviderRegistered)
}

export default registerCourseProviderSaga;