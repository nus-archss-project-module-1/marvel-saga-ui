import {call, put, takeEvery, takeLatest} from "redux-saga/effects";
import {postApi} from "../api/ApiService";
import {COURSE_PROVIDER_LOGIN, COURSE_PROVIDER_LOGIN_STATUS} from "../actions/ActionConstants";
import urls from "../api/ApiEndpoints.json";

// Worker
function* courseProviderLoginStatus(action) {
    try {
        console.log("invoking endpoint", action.payload.loginDetailsCourse)
        let URL = urls[process.env.URL_CONFIG].courseProviderUrl + '/marvel-course-provider/login';
        console.log("Course provider login URL: ", URL)

        const registeredCourseProvider = yield call(postApi, URL, action.payload.loginDetailsCourse);
        console.log("Course Provider Login:", registeredCourseProvider.data.message);
        yield put({
            type: COURSE_PROVIDER_LOGIN_STATUS,
            json: registeredCourseProvider.data.message || [{error: registeredCourseProvider.data.message}],
            courseProviderLoginId: registeredCourseProvider.data.id,
            courseProviderLoginName: registeredCourseProvider.data.name,
        });
    } catch (err) {
        console.log(err.response.data.message)
        yield put({type: COURSE_PROVIDER_LOGIN_STATUS, json: err.response.data.message});
    }
}

// Watcher
export function* courseProviderLoginSaga() {
    yield takeLatest(COURSE_PROVIDER_LOGIN, courseProviderLoginStatus)
}

export default courseProviderLoginSaga;