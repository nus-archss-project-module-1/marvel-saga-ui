// Worker
import {call, put, takeEvery} from "redux-saga/effects";
import {COURSE_REGISTRATION_STATUS, REGISTER_COURSE} from "../actions/ActionConstants";
import {postApi} from "../api/ApiService";
import urls from "../api/ApiEndpoints.json";

// Worker
function* courseRegistered(action) {
    try {
        console.log("invoking endpoint", action.payload.course)
        let URL = urls[process.env.URL_CONFIG].courseProviderUrl + '/marvel-course-provider/newCourse';
        console.log("Register course URL: ", URL);

        const registeredCourse = yield call(postApi, URL, action.payload.course);
        console.log("Course Registered:", registeredCourse.data.message);
        yield put({
            type: COURSE_REGISTRATION_STATUS,
            json: registeredCourse.data.message || [{error: registeredCourse.message}],
            isRegistered: true
        });
    } catch (err) {
        console.log(err.response.data.message)
        yield put({type: COURSE_REGISTRATION_STATUS, json: err.response.data.message, isRegistered: false});
    }
}

// Watcher
export function* registerCourseSaga() {
    console.log("Saga registerCourse")
    yield takeEvery(REGISTER_COURSE, courseRegistered)
}

export default registerCourseSaga;