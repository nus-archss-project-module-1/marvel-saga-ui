import registerStudentSaga from './registerStudentSaga'
import registerCourseProviderSaga from "./registerCourseProviderSaga";
import {fork} from "redux-saga/effects";
import registerCourseSaga from "./registerCourseSaga";
import courseProviderLoginSaga from "./courseProviderLoginSaga";
import studentLoginSaga from "./studentLoginSaga";

export default function* rootSaga() {
    yield fork(registerCourseSaga);
    yield fork(courseProviderLoginSaga);
    yield fork(registerCourseProviderSaga);
    yield fork(registerStudentSaga);
    yield fork(studentLoginSaga);
}
