import {call, put, takeEvery} from "redux-saga/effects";
import {postApi} from "../api/ApiService";
import {REGISTER_STUDENT, STUDENT_LOGIN_STATUS, STUDENT_REGISTRATION_STATUS} from "../actions/ActionConstants";
import urls from "../api/ApiEndpoints.json";

function* studentRegistered(action) {
    try {
        let URL = urls[process.env.URL_CONFIG].studentsUrl + '/marvel-student/registerStudent';
        console.log("Student registration url: ", URL);
        console.log("postApi: ", postApi);
        console.log("action.payload.user: ", action.payload.user);
        const registeredStudent = yield call(postApi, URL, action.payload.user);
        console.log("Student Registered:", registeredStudent);
        yield put({
            type: STUDENT_REGISTRATION_STATUS,
            json: registeredStudent.data.studentName || [{error: registeredStudent.data.message}],
            isRegistered: true
        });
    } catch (err) {
        console.log('error coming here: ', err)
        yield put({type: STUDENT_REGISTRATION_STATUS, json: err, isRegistered: false});
        // yield put({type: STUDENT_REGISTRATION_STATUS, json: err.response.data.studentName, isRegistered: false});
    }
}

export function* registerStudentSaga() {
    console.log("Saga registerStudent")
    yield takeEvery(REGISTER_STUDENT, studentRegistered)
}

export default registerStudentSaga;