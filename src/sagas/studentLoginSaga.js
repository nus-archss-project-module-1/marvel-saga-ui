import {call, put, takeLatest} from "redux-saga/effects";
import {postApi} from "../api/ApiService";
import {STUDENT_LOGIN, STUDENT_LOGIN_STATUS} from "../actions/ActionConstants";
import urls from '../api/ApiEndpoints.json'

// Worker
function* studentLoginStatus(action) {
    try {
        console.log("invoking endpoint", action.payload.loginDetails)
        let URL = urls[process.env.URL_CONFIG].studentsUrl + '/marvel-student/login';
        console.log("Student login url: ", URL)

        const studentLoggedIn = yield call(postApi, URL, action.payload.loginDetails);

        console.log("studentLoggedIn:", studentLoggedIn);

        console.log("Student Login:", studentLoggedIn.data.message);
        yield put({
            type: STUDENT_LOGIN_STATUS,
            json: studentLoggedIn.data.message || [{error: studentLoggedIn.data.message}],
            studentLoginId: studentLoggedIn.data.studentId,
            studentLoginName: studentLoggedIn.data.studentName,
        });
    } catch (err) {
        console.log('error coming here: ', err)
        yield put({type: STUDENT_LOGIN_STATUS, json: err});
        //console.log(err.response.data.message)
        // yield put({type: STUDENT_LOGIN_STATUS, json: err.response.data.message});
    }
}

// Watcher
export function* studentLoginSaga() {
    yield takeLatest(STUDENT_LOGIN, studentLoginStatus)
}

export default studentLoginSaga;