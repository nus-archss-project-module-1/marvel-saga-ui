import React from 'react';
import createSagaMiddleware from 'redux-saga';
import {render} from 'react-dom';
import {applyMiddleware, createStore} from 'redux';
import {Provider} from 'react-redux';
import {logger} from 'redux-logger';
import reducer from './reducers';
import MarvelStudentApp from './components/MarvelStudentApp';
import rootSaga from './sagas';
import {composeWithDevTools} from 'redux-devtools-extension';
import MarvelUiApp from "./core/MarvelUiApp";

import Amplify from 'aws-amplify';
import config from './aws-exports';
//import { Auth, Hub } from 'aws-amplify';

Amplify.configure(config)

const sagaMiddleware = createSagaMiddleware();

const store = createStore(
    reducer,
    composeWithDevTools(applyMiddleware(sagaMiddleware, logger)),
);

sagaMiddleware.run(rootSaga);

render(
    <Provider store={store}>
        <MarvelUiApp/>
    </Provider>,
    document.getElementById('root'),
);

if (module.hot) {
    module.hot.accept(MarvelStudentApp);
}

