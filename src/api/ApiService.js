import axios from 'axios';

export const postApi = async (endpoint, payloadData) => {
    console.debug("postApi payloadData:", payloadData);
    console.debug("payloadData.moduleType:", payloadData.moduleType);

    if(payloadData.moduleType === "student")
    {
        axios.defaults.headers.post['marvel-student-request'] = payloadData.token;
    }
    else if(payloadData.moduleType === "course")
    {
        axios.defaults.headers.post['marvel-course-provider-request'] = payloadData.token;
    }
    else
    {
        // do nothing
    }

    let response = await axios.post(endpoint, payloadData, {
        headers: {
            "Authorization": payloadData.token,
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
            "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token"
        }
    })
        .then((response) => {
            // console.debug("endpoint result:", response.data);
            return response;
        }).catch((error) => {
            console.debug("endpoint postApi error:", error.data);
            return Promise.reject(error);
        });

    return Promise.resolve(response);
};

export const getApi = async (endpoint, payloadData) => {
    console.debug("getApi payloadData:", payloadData);
    console.debug("payloadData.moduleType:", payloadData.moduleType);

    if(payloadData.moduleType === "student")
    {
        axios.defaults.headers.post['marvel-student-request'] = payloadData.token;
    }
    else if(payloadData.moduleType === "course")
    {
        axios.defaults.headers.post['marvel-course-provider-request'] = payloadData.token;
    }
    else
    {
        // do nothing
    }

    let response = await axios.get(endpoint, {
        headers: {
            "Authorization": payloadData.token,
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
            "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token"
        }
    })
        .then((response) => {
            // console.debug("endpoint result:", response.data);
            return response;
        }).catch((error) => {
            console.debug("endpoint getApi error:", error.data);
            return Promise.reject(error);
        });

    return Promise.resolve(response);
};