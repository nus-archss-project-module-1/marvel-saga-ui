import {
    COURSE_PROVIDER_LOGIN,
    COURSE_PROVIDER_LOGIN_STATUS,
    COURSE_PROVIDER_REGISTRATION_STATUS,
    COURSE_REGISTRATION_STATUS,
    REGISTER_COURSE,
    REGISTER_COURSE_PROVIDER,
    REGISTER_STUDENT,
    STUDENT_LOGIN,
    STUDENT_LOGIN_STATUS,
    STUDENT_REGISTRATION_STATUS
} from "../actions/ActionConstants";

const reducer = (state = {}, action) => {
    switch (action.type) {
        case REGISTER_STUDENT:
            console.log("Invoking register student: ", action.payload.user);
            return {...state, payload: action.payload.user, loading: true, isRegistered: false};

        case STUDENT_REGISTRATION_STATUS:
            return {
                ...state,
                studentRegistrationConfirmation: action.json,
                loading: false,
                isRegistered: action.isRegistered
            }

        case REGISTER_COURSE_PROVIDER:
            console.log("Invoking register course provider: ", action.payload.courseProvider);
            return {...state, payload: action.payload.courseProvider, loading: true, isRegistered: false};

        case COURSE_PROVIDER_REGISTRATION_STATUS:
            return {
                ...state,
                courseProviderRegistrationConfirmation: action.json,
                loading: false,
                isRegistered: action.isRegistered
            }

        case COURSE_PROVIDER_LOGIN:
            console.log("Invoking course provider login: ", action.payload.loginDetailsCourse);
            return {...state, payload: action.payload.loginDetailsCourse, loading: true};

        case COURSE_PROVIDER_LOGIN_STATUS:
            return {
                ...state,
                loading: false,
                isCourseProviderLoginSuccessful: action.json,
                courseProviderLoginId: action.courseProviderLoginId,
                courseProviderLoginName: action.courseProviderLoginName
            }

        case REGISTER_COURSE:
            console.log("Invoking register course: ", action.payload.course);
            return {...state, payload: action.payload.course, loading: true, isRegistered: false};

        case COURSE_REGISTRATION_STATUS:
            return {
                ...state,
                courseRegistrationConfirmation: action.json,
                loading: false,
                isRegistered: action.isRegistered,
                courseProviderLoginId: action.courseProviderLoginId,
                courseProviderLoginName: action.courseProviderLoginName,
            }

        case STUDENT_LOGIN:
            console.log("Invoking student login: ", action.payload.loginDetails);
            return {...state, payload: action.payload.loginDetails, loading: true};

        case STUDENT_LOGIN_STATUS:
            return {
                ...state,
                loading: false,
                isStudentLoginSuccessful: action.json,
                studentLoginId: action.studentLoginId,
                studentLoginName: action.studentLoginName,
            }
        default:
            return state;
    }
};

export default reducer;
