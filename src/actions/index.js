import {
    COURSE_PROVIDER_LOGIN,
    COURSE_PROVIDER_LOGIN_STATUS,
    COURSE_PROVIDER_REGISTRATION_STATUS,
    COURSE_REGISTRATION_STATUS,
    REGISTER_COURSE,
    REGISTER_COURSE_PROVIDER,
    REGISTER_STUDENT,
    STUDENT_REGISTRATION_STATUS
} from "./ActionConstants";

export const registerStudent = () => ({type: REGISTER_STUDENT});
export const studentRegistered = () => ({type: STUDENT_REGISTRATION_STATUS});

export const registerCourseProvider = () => ({type: REGISTER_COURSE_PROVIDER});
export const courseProviderRegistered = () => ({type: COURSE_PROVIDER_REGISTRATION_STATUS});

export const courseProviderLogin = () => ({type: COURSE_PROVIDER_LOGIN});
export const courseProviderLoginStatus = () => ({type: COURSE_PROVIDER_LOGIN_STATUS});


export const registerCourse = () => ({type: REGISTER_COURSE});
export const courseRegistered = () => ({type: COURSE_REGISTRATION_STATUS});