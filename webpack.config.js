const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');

module.exports = (env) => {
    return {
        resolve: {
            extensions: ['.mjs', '.js', '.jsx', '.css'],
        },

        plugins: [
            new HtmlWebpackPlugin({
                template: './src/index.html',
            }),
            new webpack.EnvironmentPlugin({
                NODE_ENV: 'development',
                URL_CONFIG: env.mode
            }),
            new webpack.NamedModulesPlugin(),
            new webpack.HotModuleReplacementPlugin(),
        ],

        devtool: 'inline-source-map',
        devServer: {
            host: '0.0.0.0',
            port: 8080,
            disableHostCheck: true,
            hot: true,
        },

        module: {
            rules: [
                {test: /\.css$/, use: ['style-loader', 'css-loader']},
                {test: /\.js$/, exclude: /node_modules/, loader: 'babel-loader'},
                {test: /\.jsx$/, exclude: /node_modules/, loader: 'babel-loader'},
                {
                    test: /\.html$/,
                    use: [
                        {
                            loader: 'html-loader',
                            options: {minimize: true},
                        },
                    ],
                },
                {
                    test: /\.(png|jpg|gif)$/,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {}
                        }
                    ]
                },
            ],
        },
    }
};
